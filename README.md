# Neptune Waves

A metadata server.  
Neptune, the god of the sea, controls the (data)waves.  
The project uses poetry (https://python-poetry.org).   
The server is based on https://fastapi.tiangolo.com.  
The documentation is here: [docs](waves/docs/index.md) .

## Getting started

It uses a MongoDB, which can be started, for example, in this way:
```
$ docker pull mongo
$ docker run -d -p 27017:27017 --name suds-mongo -e MONGO_INITDB_ROOT_USERNAME=mongoUser -e MONGO_INITDB_ROOT_PASSWORD=mongoPW -e MONGO_INITDB_DATABASE=mongoDB mongo:latest
```
It is possible to connect ot the DB using the client mongosh:  
```commandline
mongosh "mongodb://mongoUser:mongoPW@localhost:27017/mongoDB?authSource=admin"
```
Then, in order to enable the text based search on sessions, it is necessary to issue the following command:
```commandline
mongoDB> db.sessions.createIndex( { "$**": "text" } )
```

## Deployment

The code has been tested with python 3.10, 3.11, 3.12.
You can deploy it in this way:
```commandline
python -m pip install git+https://gitlab.com/surf-dms/neptune_project/neptune_server.git@devel
```
The server expects to locate the config file in /etc/waves/server.config, so you can start it in this way:
```commandline
python -m uvicorn waves.main:app --ssl-keyfile /path/to/privkey.pem --ssl-certfile /path/to/cert.pem --host 0.0.0.0 --port 1234 --reload
```

## Configuration

There is a server configuration file, which must be placed in /etc/waves/server.config:
```
[default]
mongo_user=mongoUser
mongo_password=mongoPW
mongo_host=localhost
mongo_port=27017
mongo_db=mongoDB
mongo_query=authSource=admin
neptune_server_friendly_name=neptune
neptune_server_hostname=localhost
neptune_server_port=8080
neptune_server_protocol=http
neptune_server_admin=admin
neptune_server_passwd=admin
neptune_scheduler_session_lcti=60
cert_path=
private_key_path=
```
It contains the DB credentials and url and the neptune admin credentials and "friendly name".  
See docs/examples/configuration for more examples about how to register data providers, data users ...   

## Development

The Neptune openapi client api library has been generated using https://github.com/openapi-generators/openapi-python-client:
```
openapi-generator-cli generate -g python -i openapi.json -o neptune/neptune_api_client
```
where openapi.json is the file generated automatically by FastAPI.

## Running in a container

The poetry pyproject.toml has a section for the [Poetry Dockerize Plugin](https://github.com/nicoloboschi/poetry-dockerize-plugin).  
If you deploy the plugin, then running the command:  
```commandline
poetry dockerize
```
will generate a docker image of the server.  
A docker container can use the image in this way:
```commandline
docker run --privileged=true -p 8080:8080 --mount type=bind,source=/path/to/server.config,target=/etc/waves/server.config -it --net="host" --name neptune_server waves:latest
```
the option "-p" is ignored when used with "--net=host".