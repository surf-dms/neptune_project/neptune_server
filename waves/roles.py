from fastapi import Depends, HTTPException, Request
from typing import List

from . import schemas


class RoleChecker:
    def __init__(self, allowed_roles: List):
        self.allowed_roles = allowed_roles

    def __call__(self, request: Request):
        data_user = request.state.data_user_db
        if data_user.role not in self.allowed_roles:
            #logger.debug(f"User with role {user.role} not in {self.allowed_roles}")
            raise HTTPException(status_code=403, detail="Operation not permitted")
