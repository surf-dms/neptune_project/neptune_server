import os
import configparser
import sys

from pydantic_settings import BaseSettings

from . import constants


def config_section_map(config, section):
    dict1 = {}
    options = config.options(section)
    for option in options:
        dict1[option] = config.get(section, option)
    return dict1


def parse_configuration(conf_path=constants.SERVER_CONFIG_PATH):
    conf = ''
    conf_dict = {}
    NEPTUNE_SERVER_CONFIG_PATH = os.getenv("NEPTUNE_SERVER_CONFIG_PATH", '')
    if NEPTUNE_SERVER_CONFIG_PATH and len(NEPTUNE_SERVER_CONFIG_PATH)>0:
        conf_path = NEPTUNE_SERVER_CONFIG_PATH
    if os.path.exists(conf_path):
        conf = conf_path
    else:
        return "The configuration file {} doesn't exist".format(conf_path), conf_dict
    config = configparser.ConfigParser()
    config.read(conf)
    conf_dict["default"] = config.defaults()
    for section in config.sections():
        conf_dict[section] = config_section_map(config, section)
    return None, conf_dict


msg, config_dict = parse_configuration()
if msg:
    print(msg)
    sys.exit(1)


# class Settings(BaseSettings):
#     mongo_user: str = config_dict['default']['mongo_user']
#     mongo_password: str = config_dict['default']['mongo_password']
#     mongo_host: str = config_dict['default']['mongo_host']
#     mongo_port: int = config_dict['default']['mongo_port']
#     mongo_db: str = config_dict['default']['mongo_db']
#     mongo_query: str = config_dict['default']['mongo_query']
#     neptune_server_friendly_name: str = config_dict['default']['neptune_server_friendly_name']
#     neptune_server_admin: str = config_dict['default']['neptune_server_admin']
#     neptune_server_passwd: str = config_dict['default']['neptune_server_passwd']
#     neptune_server_hostname: str = config_dict['default']['neptune_server_hostname']
#     neptune_server_port: int = config_dict['default']['neptune_server_port']
#     neptune_server_protocol: str = config_dict['default']['neptune_server_protocol']
#     # neptune scheduler session lifetime check trigger interval (in seconds)
#     neptune_scheduler_session_lcti: int = config_dict['default']['neptune_scheduler_session_lcti']
#     cert_path: str = config_dict['default']['cert_path']
#     private_key_path: str = config_dict['default']['private_key_path']

