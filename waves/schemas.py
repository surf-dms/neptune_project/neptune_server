import datetime
import uuid

from beanie import Document
from typing import List, Union, Annotated, Tuple, Optional, Dict, Any
from pydantic import BaseModel, Field, Json
from beanie import PydanticObjectId
from typing_extensions import Literal

from . import constants


class Endpoint(BaseModel):
    hostname: str
    port: int
    protocol: constants.EP_PROTOCOLS
    ep_type: constants.EP_TYPES = constants.EP_DEFAULT


class IrodsEndpoint(Endpoint):
    zone: str
    protocol: constants.IRODS_EP_PROTOCOLS
    ep_type: constants.IRODS_EP_TYPES = constants.IRODS_EP_DEFAULT


EndpointModel = Annotated[Union[Endpoint, IrodsEndpoint], Field(discriminator='ep_type')]


class ProviderBase(Document):
    friendly_name: str
    endpoints: List[EndpointModel] = []
    endpoint_default_type: constants.PROVIDER_EP_DEFAULT_TYPES = constants.EP_DEFAULT
    administrators: list[PydanticObjectId] = []


class ProviderCreate(ProviderBase):
    pass


class Provider(ProviderCreate):
    id: PydanticObjectId
    create_ts: datetime.datetime
    modify_ts: datetime.datetime

    class Settings:
        name = "providers"

    class Config:
        arbitrary_types_allowed = True


class ProjectBase(Document):
    name: str
    description: str | None = None
    members: list[PydanticObjectId] = []
    expiration_ts: datetime.datetime
    providers: list[PydanticObjectId] = []
    service_accounts: Dict[PydanticObjectId, PydanticObjectId] = dict()
    sram_enabled: bool = False
    sram_api_key: str | None = None
    sram_administrators: list[str] = []
    sram_logo: str = constants.NEPTUNE_LOGO
    sram_sender: str | None = None


class ProjectCreate(ProjectBase):
    pass


class Project(ProjectCreate):
    id: PydanticObjectId
    sram_co_identifier: str | None = None
    create_ts: datetime.datetime
    modify_ts: datetime.datetime

    class Settings:
        name = "projects"

    class Config:
        arbitrary_types_allowed = True


class UserBase(Document):
    surf_id: str
    email: str
    data_users: list[PydanticObjectId] = []

    class Config:
        arbitrary_types_allowed = True


class UserCreate(UserBase):
    pass


class User(UserCreate):
    id: PydanticObjectId
    create_ts: datetime.datetime
    modify_ts: datetime.datetime

    class Settings:
        name = "users"

    class Config:
        arbitrary_types_allowed = True


class DataUserBase(Document):
    username: str
    username_source: constants.USERNAME_SOURCES
    provider_id: PydanticObjectId
    user_id: list[PydanticObjectId] = []
    relationships: list[PydanticObjectId] = []
    password: str = None
    role: constants.DATA_USER_ROLES = constants.DATA_USER_ROLE_DEFAULT
    public_key: str | None = None

    class Config:
        arbitrary_types_allowed = True


class DataUserCreate(DataUserBase):
    pass


class DataUser(DataUserCreate):
    id: PydanticObjectId
    create_ts: datetime.datetime
    modify_ts: datetime.datetime

    class Settings:
        name = "data_users"

    class Config:
        arbitrary_types_allowed = True


class DataBase(Document):
    key_in_provider: str
    provider_id: PydanticObjectId
    friendly_name: str
    submitted_usernames: list[PydanticObjectId] = []
    relationships: list[PydanticObjectId] = []
    access: Literal['public', 'private']

    class Config:
        arbitrary_types_allowed = True


class DataCreate(DataBase):
    pass


class Data(DataCreate):
    id: PydanticObjectId
    create_ts: datetime.datetime
    modify_ts: datetime.datetime

    class Settings:
        name = "data"

    class Config:
        arbitrary_types_allowed = True


class MetaDataBase(Document):
    provider_md_id: str | None = ...
    provider_id: PydanticObjectId | None = ...
    content: str | None = ...
    content_type: constants.METADATA_CONTENT_TYPES = constants.METADATA_CONTENT_TYPE_DEFAULT
    relationships: list[PydanticObjectId] = []
    data: list[PydanticObjectId] = []
    access: constants.METADATA_ACCESS_TYPES = constants.METADATA_ACCESS_DEFAULT

    class Config:
        arbitrary_types_allowed = True


class MetaDataCreate(MetaDataBase):
    unique_name: str
    data_users: list[PydanticObjectId] = []
    sessions: list[PydanticObjectId] = []


class MetaData(MetaDataCreate):
    id: PydanticObjectId
    create_ts: datetime.datetime
    modify_ts: datetime.datetime

    class Settings:
        name = "metadata"

    class Config:
        arbitrary_types_allowed = True


class RelationshipBase(Document):
    permission: Literal['read', 'write']
    data_user: PydanticObjectId
    data: PydanticObjectId

    class Config:
        arbitrary_types_allowed = True


class RelationshipCreate(RelationshipBase):
    pass


class Relationship(RelationshipCreate):
    id: PydanticObjectId
    create_ts: datetime.datetime
    modify_ts: datetime.datetime

    class Settings:
        name = "relationships"

    class Config:
        arbitrary_types_allowed = True


class Profile(BaseModel):
    tag: str
    username: str | None = None
    # Field(repr=True)
    secret: Optional[str] = None
    secret_type: constants.PROFILE_SECRET_TYPES
    auth_scheme: constants.PROFILE_AUTH_SCHEMES = constants.PROFILE_AUTH_SCHEME_DEFAULT
    endpoint: Endpoint | IrodsEndpoint = Field(discriminator='ep_type')
    metadata: Dict[str, Union[Dict, List]] = dict()


class Event(BaseModel):
    operation: constants.OPERATION_TYPES
    path: str
    target_path: str | None = None
    #metadata: list[PydanticObjectId] = []
    metadata: List[Tuple[str, str, Union[Dict, List]]] = []
    ticket: str | None = None
    profile_tags: Dict[str, str] = {}


class SessionBase(Document):
    events: List[Event] = []
    lifetime: int = 3600
    access: constants.SESSION_ACCESS_TYPES = constants.SESSION_ACCESS_TYPE_DEFAULT
    participants: List[PydanticObjectId] = []
    profiles: List[Profile] = []
    provenance: List[str] = []

    class Config:
        arbitrary_types_allowed = True


class SessionCreate(SessionBase):
    code: uuid.UUID = Field(default_factory=lambda: uuid.uuid4())
    status: constants.SESSION_STATES
    data_user: PydanticObjectId

    class Config:
        arbitrary_types_allowed = True


class Session(SessionCreate):
    id: PydanticObjectId
    create_ts: datetime.datetime
    modify_ts: datetime.datetime

    class Settings:
        name = "sessions"

    class Config:
        arbitrary_types_allowed = True
