import os
import motor.motor_asyncio
from beanie import init_beanie
from beanie import PydanticObjectId
from datetime import datetime
from pydantic_settings import BaseSettings

from . import helpers, constants, guard
from .schemas import Provider, Endpoint, User, DataUser, Data, Relationship, Session, Project



async def create_admin_user(provider_id, neptune_admin, neptune_admin_password):
    hashed_password = guard.hash_bcrypt(neptune_admin_password)
    db_admin_user = DataUser(id=PydanticObjectId(),
                             username=neptune_admin,
                             username_source='local',
                             provider_id=provider_id,
                             user_id=[],
                             password=hashed_password,
                             role='admin',
                             create_ts=datetime.now(),
                             modify_ts=datetime.now())
    await db_admin_user.create()


#async def init_db(settings: BaseSettings):
async def init_db(config: dict):

#    MONGO_DETAILS = (f"mongodb://{settings.mongo_user}:{settings.mongo_password}"
#                     f"@{settings.mongo_host}:{settings.mongo_port}/{settings.mongo_db}?{settings.mongo_query}")
    MONGO_DETAILS = (f"mongodb://{config['mongo_user']}:{config['mongo_password']}"
                     f"@{config['mongo_host']}:{config['mongo_port']}/{config['mongo_db']}?{config['mongo_query']}")
    client = motor.motor_asyncio.AsyncIOMotorClient(MONGO_DETAILS)

#    await init_beanie(database=client.get_database(name=settings.mongo_db),
#                      document_models=[Provider, User, DataUser, Data, Relationship, Session, Project])
    await init_beanie(database=client.get_database(name=config['mongo_db']),
                      document_models=[Provider, User, DataUser, Data, Relationship, Session, Project])

#    neptune_name = settings.neptune_server_friendly_name
#    neptune_admin = settings.neptune_server_admin
    neptune_name = config['neptune_server_friendly_name']
    neptune_admin = config['neptune_server_admin']
    provider = await Provider.find(Provider.friendly_name == neptune_name).first_or_none()
    if provider:
        print("provider {} found".format(neptune_name))
        admin_user = await DataUser.find(DataUser.username == neptune_admin, DataUser.provider_id == provider.id).first_or_none()
        if admin_user:
            print("admin user {} found".format(neptune_admin))
        else:
#            await create_admin_user(provider_id=provider.id, neptune_admin=neptune_admin,
#                                    neptune_admin_password=settings.neptune_server_passwd)
            await create_admin_user(provider_id=provider.id, neptune_admin=neptune_admin,
                                    neptune_admin_password=config['neptune_server_passwd'])
    else:
        # ep = Endpoint(hostname=settings.neptune_server_hostname,
        #               port=settings.neptune_server_port,
        #               protocol=settings.neptune_server_protocol,
        #               ep_type='web portal')
        ep = Endpoint(hostname=config['neptune_server_hostname'],
                      port=int(config['neptune_server_port']),
                      protocol=config['neptune_server_protocol'],
                      ep_type='web portal')
        db_provider = Provider(id=PydanticObjectId(),
                               friendly_name=neptune_name,
                               endpoints=[ep],
                               create_ts=datetime.now(),
                               modify_ts=datetime.now())
        await db_provider.create()
        # await create_admin_user(provider_id=db_provider.id, neptune_admin=neptune_admin,
        #                         neptune_admin_password=settings.neptune_server_passwd)
        await create_admin_user(provider_id=db_provider.id, neptune_admin=neptune_admin,
                                neptune_admin_password=config['neptune_server_passwd'])
        
