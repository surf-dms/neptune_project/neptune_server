import json
import datetime
import logging
import time
import requests


class sram_api_client():
    log = logging.getLogger(__name__)

    def __init__(self, sram_api_key, sram_api_url):
        self.log.debug("initialize sram api client")
        self.api_key=sram_api_key
        self.url=sram_api_url

    def sram_get_collaboration_by_name(self, co_name):
        api_url = "{}/api/organisations/v1".format(self.url)
        headers = {'Content-Type': 'application/json', 'charset': 'UTF-8',
                   'Authorization': 'bearer ' + self.api_key}
        response = requests.get(api_url, headers=headers, timeout=30, verify=True)
        org = response.json()
        for co in org['collaborations']:
            if co['name'] == co_name:
                return co
        return None

    def sram_post_collaboration(self, collaboration_name, description, disable_join_requests=True,
                                administrators=[], logo=None):
        api_url = "{}/api/collaborations/v1".format(self.url)
        headers = {'Content-Type': 'application/json', 'charset': 'UTF-8', 'Authorization': 'bearer ' + self.api_key}
        disable_join_requests = True
        payload = {
            "name": collaboration_name,
            "description": description,
            "disable_join_requests": disable_join_requests,
            "disclose_member_information": True,
            "disclose_email_information": True,
            "administrators": administrators,
            "logo": logo,
            "tags": []
        }
        response = requests.post(api_url, json=payload, headers=headers, timeout=30, verify=True)
        return response.json()

    def sram_get_co_members(self, co_identifier):
        api_url = "{}/api/collaborations/v1/{}".format(self.url, co_identifier)
        headers = {'Content-Type': 'application/json', 'charset': 'UTF-8', 'Authorization': 'bearer ' + self.api_key}
        response = requests.get(api_url, headers=headers, timeout=30, verify=True)
        data = response.json()
        co_members = []
        return data['collaboration_memberships']

    def verify_user_membership_by_email(self, co_id, email):
        co_members = self.sram_get_co_members(co_identifier=co_id)
        co_members_emails = []
        for co_member in co_members:
            co_members_emails.append(co_member['user']['email'])
        return email in co_members_emails

    def sram_put_collaboration_invitation(self, username, co_identifier, project_name=None, sender=None):
        api_url = "{}/api/invitations/v1/collaboration_invites".format(self.url)
        headers = {'Content-Type': 'application/json', 'charset': 'UTF-8', 'Authorization': 'bearer ' + self.api_key}
        # Now plus a year.
        expiration_date = datetime.datetime.fromtimestamp(int(time.time() + 3600 * 24 * 365)).strftime('%Y-%m-%d')
        # Get epoch expiry date.
        date = datetime.datetime.strptime(expiration_date, "%Y-%m-%d").replace(tzinfo=datetime.timezone.utc)
        #epoch = datetime.datetime.utcfromtimestamp(0)
        epoch = datetime.datetime.fromtimestamp(0, datetime.timezone.utc)
        epoch_date = int((date - epoch).total_seconds()) * 1000
        # Build SRAM payload.
        project_name = project_name if project_name else 'a project'
        sender_name = sender if sender else project_name
        payload = {
            "collaboration_identifier": co_identifier,
            "message": "Invitation to register to {}".format(project_name),
            "intended_role": "member",
            "sender_name": sender_name,
            "invitation_expiry_date": epoch_date,
            "invites": [username],
            "groups": []
        }
        response = requests.put(api_url, json=payload, headers=headers, timeout=30, verify=True)
        return response.status_code == 201

    def sram_delete_collaboration(self, co_identifier):
        api_url = "{}/api/collaborations/v1/{}".format(self.url, co_identifier)
        headers = {'Content-Type': 'application/json', 'charset': 'UTF-8', 'Authorization': 'bearer ' + self.api_key}
        response = requests.delete(api_url, headers=headers, timeout=30, verify=True)
        return response.status_code == 204

    def sram_get_co_member_uid(self, co_identifier, email):
        uid = None
        co_members = self.sram_get_co_members(co_identifier=co_identifier)
        for co_member in co_members:
            sram_name = co_member['user']['email']
            if email == sram_name.lower():
                uid = co_member['user']['uid']
        return uid

    def sram_delete_collaboration_membership(self, co_identifier, user_uid):
        api_url = "{}/api/collaborations/v1/{}/members/{}".format(self.url, co_identifier, user_uid)
        headers = {'Content-Type': 'application/json', 'charset': 'UTF-8', 'Authorization': 'bearer ' + self.api_key}
        response = requests.delete(api_url, headers=headers, timeout=30, verify=True)
        return response.status_code == 204