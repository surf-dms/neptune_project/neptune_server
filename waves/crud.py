import json
import uuid
import hashlib
from datetime import datetime, timedelta
from enum import Enum

from beanie import PydanticObjectId
from beanie.odm.operators.find.comparison import In
from beanie.odm.operators.find.element import Exists
from beanie.odm.operators.find.evaluation import Text, JsonSchema
from beanie.odm.operators.find.logical import Or, And
from fastapi import HTTPException
from typing_extensions import Any

from waves.sramwave import sram_api_client
from . import schemas, guard, helpers, constants


class ProvenanceActionType(str, Enum):
    approved = "approved"
    rejected = "rejected"
    shared = "shared"
    copied = "copied"


async def get_provider(provider_id: PydanticObjectId):
    return await schemas.Provider.find(
        schemas.Provider.id == provider_id,
    ).first_or_none()


async def get_providers_by_hostname(hostname: str):
    db_providers = await get_providers()
    providers = []
    for provider in db_providers:
        for endpoint in provider.endpoints:
            if endpoint.hostname == hostname:
                providers.append(provider)
    return providers


async def get_providers_by_port(port: int):
    db_providers = await get_providers()
    providers = []
    for provider in db_providers:
        for endpoint in provider.endpoints:
            if endpoint.port == port:
                providers.append(provider)
    return providers


async def get_providers_by_protocol(protocol: str):
    db_providers = await get_providers()
    providers = []
    for provider in db_providers:
        for endpoint in provider.endpoints:
            if endpoint.protocol == protocol:
                providers.append(provider)
    return providers


async def get_providers_by_type(ep_type: str):
    db_providers = await get_providers()
    providers = []
    for provider in db_providers:
        for endpoint in provider.endpoints:
            if endpoint.ep_type == ep_type:
                providers.append(provider)
    return providers


async def get_provider_by_endpoint(eps: list[schemas.Endpoint]):
    return await schemas.Provider.find(
        In(schemas.Provider.endpoints, eps)
    ).first_or_none()


async def get_providers_by_name(name: str):
    return await schemas.Provider.find(
        schemas.Provider.friendly_name == name,
    ).to_list()


async def get_providers() -> list[schemas.Provider]:
    providers = await schemas.Provider.find_all().to_list()
    return providers


async def create_provider(provider: schemas.ProviderCreate):
    db_provider = schemas.Provider(id=PydanticObjectId(),
                                   friendly_name=provider.friendly_name,
                                   endpoints=provider.endpoints,
                                   endpoint_default_type=provider.endpoint_default_type,
                                   administrators=provider.administrators,
                                   create_ts=datetime.now(),
                                   modify_ts=datetime.now())
    result = await db_provider.create()
    return result


async def delete_provider(provider_id: PydanticObjectId):
    await schemas.Provider.find(
        schemas.Provider.id == provider_id,
    ).delete_many()


async def delete_providers():
    await schemas.Provider.delete_all()


async def get_project(project_id: PydanticObjectId):
    return await schemas.Project.find(
        schemas.Project.id == project_id,
    ).first_or_none()


async def get_project_by_name(name: str):
    return await schemas.Project.find(
        schemas.Project.name == name,
    ).first_or_none()


async def get_project_by_participant(member: PydanticObjectId):
    projects_db = await schemas.Project.find_all().to_list()
    projects = []
    for project_db in projects_db:
        if member in project_db.members:
            projects.append(project_db)
    return projects


async def get_projects():
    return await schemas.Project.find_all().to_list()


async def init_sram_client(sram_api_key: str):
    sram_provider_list = await get_providers_by_name(constants.SRAM_PROVIDER_FRIENDLY_NAME)
    if not sram_provider_list or len(sram_provider_list) == 0:
        raise HTTPException(status_code=404, detail="SRAM provider not found!")
    sram_provider = sram_provider_list.pop()
    for endpoint in sram_provider.endpoints:
        if endpoint.ep_type == sram_provider.endpoint_default_type:
            sram_api_host = endpoint.hostname
            sram_api_protocol = endpoint.protocol
            break
    sram_api_url = sram_api_protocol + '://' + sram_api_host
    return sram_api_client.sram_api_client(sram_api_key=sram_api_key, sram_api_url=sram_api_url)


async def get_project_sram_co_by_name(project_name: str, sram_api_key: str):
    sram_client = await init_sram_client(sram_api_key=sram_api_key)
    return sram_client.sram_get_collaboration_by_name(co_name=project_name)


async def verify_project_sram_user(project_name: str, email: str):
    project = await get_project_by_name(name=project_name)
    response = False
    if project:
        sram_client = await init_sram_client(sram_api_key=project.sram_api_key)
        response = sram_client.verify_user_membership_by_email(co_id=project.sram_co_identifier, email=email)
    return response


async def register_project_sram_user(project_name: str, email: str):
    project = await get_project_by_name(name=project_name)
    sram_client = await init_sram_client(sram_api_key=project.sram_api_key)
    return sram_client.sram_put_collaboration_invitation(username=email, co_identifier=project.sram_co_identifier,
                                                         project_name=project_name, sender=project.sram_sender)


async def delete_project_sram_user(project_name: str, email: str):
    project = await get_project_by_name(name=project_name)
    sram_client = await init_sram_client(sram_api_key=project.sram_api_key)
    co_member_uid = sram_client.sram_get_co_member_uid(co_identifier=project.sram_co_identifier, email=email)
    return sram_client.sram_delete_collaboration_membership(co_identifier=project.sram_co_identifier, user_uid=co_member_uid)


async def create_project(project: schemas.ProjectCreate):

    if project.sram_enabled:
        sram_co = await get_project_sram_co_by_name(project_name=project.name, sram_api_key=project.sram_api_key)
        if not sram_co:
            sram_client = await init_sram_client(sram_api_key=project.sram_api_key)
            sram_co = sram_client.sram_post_collaboration(collaboration_name=project.name, description=project.description,
                                                          administrators=project.sram_administrators)
        sram_co_id = sram_co['identifier']
    else:
        sram_co_id = None

    db_project = schemas.Project(id=PydanticObjectId(),
                                 name=project.name,
                                 description=project.description,
                                 members=project.members,
                                 expiration_ts=project.expiration_ts,
                                 providers=project.providers,
                                 service_accounts=project.service_accounts,
                                 sram_enabled=project.sram_enabled,
                                 sram_api_key=project.sram_api_key,
                                 sram_administrators=project.sram_administrators,
                                 sram_logo=project.sram_logo,
                                 sram_co_identifier=sram_co_id,
                                 create_ts=datetime.now(),
                                 modify_ts=datetime.now())
    return await db_project.create()


async def update_project(project_id: PydanticObjectId, project: schemas.ProjectCreate):
    if project.sram_enabled:
        sram_co = await get_project_sram_co_by_name(project_name=project.name, sram_api_key=project.sram_api_key)
        if not sram_co:
            sram_client = await init_sram_client(sram_api_key=project.sram_api_key)
            sram_co = sram_client.sram_post_collaboration(collaboration_name=project.name, description=project.description,
                                                          administrators=project.sram_administrators)
        sram_co_id = sram_co['identifier']
    else:
        sram_co_id = None

    req = {k: v for k, v in project.dict().items() if v is not None}
    update_query = {"$set": {
        field: value for field, value in req.items()
    }}

    project_db = await get_project(project_id)
    if not project_db:
        raise HTTPException(status_code=404, detail="Project not found!")

    update_query["$set"]["sram_co_identifier"] = sram_co_id
    update_query["$set"]["modify_ts"] = datetime.now()
    await project_db.update(update_query)
    return project_db


async def delete_project(project_id: PydanticObjectId):
    await schemas.Project.find(
        schemas.Project.id == project_id,
    ).delete_many()


async def delete_projects():
    await schemas.Project.delete_all()


async def get_user(user_id: PydanticObjectId):
    return await schemas.User.find(
        schemas.User.id == user_id,
    ).first_or_none()


async def get_user_by_surf_id(surf_id: str):
    return await schemas.User.find(
        schemas.User.surf_id == surf_id,
    ).first_or_none()


async def get_user_by_email(email: str):
    return await schemas.User.find(
        schemas.User.email == email,
    ).to_list()


async def get_users():
    return await schemas.User.find_all().to_list()


async def create_user(user: schemas.UserCreate):
    db_user = schemas.User(id=PydanticObjectId(),
                           surf_id=user.surf_id,
                           email=user.email,
                           data_users=user.data_users,
                           create_ts=datetime.now(),
                           modify_ts=datetime.now())
    return await db_user.create()


async def update_user(user_id: PydanticObjectId, user: schemas.UserCreate):
    req = {k: v for k, v in user.dict().items() if v is not None}
    update_query = {"$set": {
        field: value for field, value in req.items()
    }}

    user_db = await get_user(user_id)
    if not user_db:
        raise HTTPException(status_code=404, detail="User not found!")

    update_query["$set"]["modify_ts"] = datetime.now()
    await user_db.update(update_query)
    return user_db


async def delete_user(user_id: PydanticObjectId):
    await schemas.User.find(
        schemas.User.id == user_id,
    ).delete_many()


async def delete_users():
    await schemas.User.delete_all()


async def get_data_user(user_id: PydanticObjectId):
    return await schemas.DataUser.find(
        schemas.DataUser.id == user_id
    ).first_or_none()


async def get_data_user_by_username(username: str) -> list[schemas.DataUser]:
    return await schemas.DataUser.find(
        schemas.DataUser.username == username,
    ).to_list()


async def get_data_user_by_provider_id(provider_id: PydanticObjectId) -> list[schemas.DataUser]:
    return await schemas.DataUser.find(
        schemas.DataUser.provider_id == provider_id,
    ).to_list()


async def get_data_user_by_provider_username(username: str, provider_id: PydanticObjectId):
    return await schemas.DataUser.find(
        schemas.DataUser.username == username,
        schemas.DataUser.provider_id == provider_id,
    ).first_or_none()


async def get_data_users():
    return await schemas.DataUser.find_all().to_list()


async def create_data_user(data_user: schemas.DataUserCreate):
    hashed_password = guard.hash_bcrypt(data_user.password)
    db_data_user = schemas.DataUser(id=PydanticObjectId(),
                                    username=data_user.username,
                                    username_source=data_user.username_source,
                                    provider_id=data_user.provider_id,
                                    user_id=data_user.user_id,
                                    password=hashed_password,
                                    role=data_user.role,
                                    public_key=data_user.public_key,
                                    create_ts=datetime.now(),
                                    modify_ts=datetime.now())
    await db_data_user.create()
    return db_data_user


async def update_data_user(data_user_id: PydanticObjectId, data_user: schemas.DataUserCreate):
    req = {k: v for k, v in data_user.dict().items() if v is not None}
    if "password" in req.keys():
        req["password"] = guard.hash_bcrypt(req["password"])
    update_query = {"$set": {
        field: value for field, value in req.items()
    }}

    db_data_user = await get_data_user(data_user_id)
    if not db_data_user:
        raise HTTPException(status_code=404, detail="User not found!")

    update_query["$set"]["modify_ts"] = datetime.now()
    await db_data_user.update(update_query)
    return db_data_user


async def delete_data_user(data_user_id: PydanticObjectId):
    await schemas.DataUser.find(
        schemas.DataUser.id == data_user_id,
    ).delete_many()


async def delete_data_users(provider_id: PydanticObjectId | None = None):
    if provider_id:
        await schemas.DataUser.find(
            schemas.DataUser.provider_id == provider_id,
        ).delete_many()
    else:
        await schemas.DataUser.delete_all()


async def get_data_by_provider(provider_location: str, provider_id: PydanticObjectId):
    return await schemas.Data.find(
        schemas.Data.provider_id == provider_id,
        schemas.Data.key_in_provider == provider_location,
    ).to_list()


async def get_all_data_by_provider(provider_id: PydanticObjectId):
    return await schemas.Data.find(
        schemas.Data.provider_id == provider_id
    ).to_list()


async def get_data_by_provider_username(provider_id: PydanticObjectId, usernames: list[PydanticObjectId]):
    return await schemas.Data.find(
        schemas.Data.provider_id == provider_id,
        In(schemas.Data.submitted_usernames, usernames)
    ).to_list()


async def get_data_by_username(usernames: list[PydanticObjectId]):
    return await schemas.Data.find(
        In(schemas.Data.submitted_usernames, usernames)
    ).to_list()


async def get_data_by_email(email: str):
    user_db = await get_user_by_email(email=email)
    user_db = next(iter(user_db), None)
    return await get_data_by_username(usernames=user_db.data_users)


async def get_data_by_id(data_id: PydanticObjectId):
    return await schemas.Data.find(
        schemas.Data.id == data_id,
    ).first_or_none()  


async def get_data():
    return await schemas.Data.find_all().to_list()


async def create_data(data: schemas.DataCreate):
    db_data = schemas.Data(id=PydanticObjectId(),
                           key_in_provider=data.key_in_provider,
                           provider_id=data.provider_id,
                           friendly_name=data.friendly_name,
                           submitted_usernames=data.submitted_usernames,
                           access=data.access,
                           create_ts=datetime.now(),
                           modify_ts=datetime.now())
    await db_data.create()
    return db_data


async def update_data(data_id: PydanticObjectId, data: schemas.DataCreate):
    req = {k: v for k, v in data.dict().items() if v is not None}
    update_query = {"$set": {
        field: value for field, value in req.items()
    }}

    db_data = await get_data_by_id(data_id)
    if not db_data:
        raise HTTPException(status_code=404, detail="Data not found!")

    update_query["$set"]["modify_ts"] = datetime.now()
    await db_data.update(update_query)
    return db_data


async def delete_data(data_id: PydanticObjectId):
    await schemas.Data.find(
        schemas.Data.id == data_id,
    ).delete_many()


async def delete_all_data():
    await schemas.Data.delete_all()


async def get_metadata_by_provider_id_md_id(provider_id: PydanticObjectId, provider_md_id: str):
    return await schemas.MetaData.find(
        schemas.MetaData.provider_id == provider_id,
        schemas.MetaData.provider_md_id == provider_md_id,
    ).first_or_none()


async def get_all_metadata_by_provider(provider_id: PydanticObjectId):
    return await schemas.MetaData.find(
        schemas.MetaData.provider_id == provider_id
    ).to_list()


async def get_metadata_by_session(sessions: list[PydanticObjectId]):
    return await schemas.MetaData.find(
        In(schemas.MetaData.sessions, sessions)
    ).to_list()


async def get_metadata_by_provider_username(provider_id: PydanticObjectId, usernames: list[PydanticObjectId]):
    return await schemas.MetaData.find(
        schemas.MetaData.provider_id == provider_id,
        In(schemas.MetaData.data_users, usernames)
    ).to_list()


async def get_metadata_by_provider_md_id(provider_md_id: str):
    return await schemas.MetaData.find(
        schemas.MetaData.provider_md_id == provider_md_id,
    ).first_or_none()


async def get_metadata_by_unique_name(unique_name: str):
    return await schemas.MetaData.find(
        schemas.MetaData.unique_name == unique_name,
    ).first_or_none()


async def get_metadata_by_id(metadata_id: PydanticObjectId):
    return await schemas.MetaData.find(
        schemas.MetaData.id == metadata_id,
    ).first_or_none()


async def get_metadata():
    return await schemas.MetaData.find_all().to_list()


async def create_metadata(metadata: schemas.MetaDataBase, data_users: list[PydanticObjectId]):
    unique_name = helpers.get_metadata_unique_name(content=metadata.content, 
                                                   meta_id=metadata.provider_md_id, 
                                                   provider_id=metadata.provider_id)
    db_metadata = schemas.MetaData(id=PydanticObjectId(),
                                   provider_md_id=metadata.provider_md_id,
                                   provider_id=metadata.provider_id,
                                   content=metadata.content,
                                   content_type=metadata.content_type,
                                   unique_name=unique_name,
                                   data_users=data_users,
                                   relationships=metadata.relationships,
                                   data=metadata.data,
                                   sessions=[],
                                   access=metadata.access,
                                   create_ts=datetime.now(),
                                   modify_ts=datetime.now())
    await db_metadata.create()
    return db_metadata


async def update_metadata(metadata_id: PydanticObjectId, metadata: schemas.MetaDataBase):
    req = {k: v for k, v in metadata.dict().items() if v is not None}
    if "content" in req.keys():
        req["unique_name"] = (hashlib.md5(metadata.content.encode())).hexdigest()
    elif "provider_md_id" in req.keys():
        if "provider_id" in req.keys():
            coordinates = str(metadata.provider_id) + metadata.provider_md_id
            req["unique_name"] = (hashlib.md5(metadata.coordinates.encode())).hexdigest()
        else:
            req["unique_name"] = (hashlib.md5(metadata.metadata.provider_md_id.encode())).hexdigest()
    update_query = {"$set": {
        field: value for field, value in req.items()
    }}

    db_metadata = await get_metadata_by_id(metadata_id)
    if not db_metadata:
        raise HTTPException(status_code=404, detail="Metadata not found!")

    update_query["$set"]["modify_ts"] = datetime.now()
    await db_metadata.update(update_query)
    return db_metadata


async def delete_metadata(metadata_id: PydanticObjectId):
    await schemas.MetaData.find(
        schemas.MetaData.id == metadata_id,
    ).delete_many()


async def delete_all_metadata():
    await schemas.MetaData.delete_all()


async def get_relationship(data: PydanticObjectId, data_user: PydanticObjectId):
    return await schemas.Relationship.find(
        schemas.Relationship.data == data,
        schemas.Relationship.data_user == data_user,
    ).first_or_none()


async def get_relationship_by_data(data: PydanticObjectId):
    return await schemas.Relationship.find(
        schemas.Relationship.data == str(data),
    ).to_list()


async def get_relationship_by_data_user(data_user: PydanticObjectId):
    return await schemas.Relationship.find(
        schemas.Relationship.data_user == data_user,
    ).to_list()


async def get_relationships():
    return await schemas.Relationship.find_all().to_list()


async def create_relationship(relationship: schemas.RelationshipCreate):
    db_relationship = schemas.Relationship(id=PydanticObjectId(),
                                           permission=relationship.permission,
                                           data_user=relationship.data_user,
                                           data=relationship.data,
                                           create_ts=datetime.now(),
                                           modify_ts=datetime.now())
    await db_relationship.create()
    return db_relationship


async def delete_relationship(id: PydanticObjectId):
    await schemas.Session.find(
        schemas.Relationship.id == id,
    ).delete_many()


async def delete_all_relationships():
    await schemas.Relationship.delete_all()


async def update_relationship(data_id: PydanticObjectId, relationship: schemas.RelationshipCreate):
    req = {k: v for k, v in relationship.dict().items() if v is not None}
    update_query = {"$set": {
        field: value for field, value in req.items()
    }}

    db_relationship = await get_relationship(data_id)
    if not db_relationship:
        raise HTTPException(status_code=404, detail="Relationship not found!")

    update_query["$set"]["modify_ts"] = datetime.now()
    await db_relationship.update(update_query)
    return db_relationship


async def get_session_by_query(main_query: Any, filter_text: str = None, filter_json: str = None,
                               status: constants.SESSION_STATES = None,
                               access: constants.SESSION_ACCESS_TYPES = None, time_interval: timedelta = None,
                               author: str = None, neptune_name: str = None):
    filter_and_conditions = []
    if status:
        filter_and_conditions.append({'status': status})
    if access:
        filter_and_conditions.append({'access': access})
    if author:
        providers_list = await get_providers_by_name(name=neptune_name)
        data_user = await get_data_user_by_provider_username(username=author, provider_id=providers_list.pop().id)
        data_user_id = data_user.id if data_user else ""
        filter_and_conditions.append({'data_user': data_user_id})
    if time_interval:
        time_source_date = datetime.now() - time_interval
        filter_and_conditions.append({'create_ts': {"$gt": time_source_date}})
    if filter_json:
        try:
            filter_and_conditions.append(json.loads(filter_json))
        except json.JSONDecodeError as e:
            raise HTTPException(status_code=400, detail="Malformed json: {}".format(filter_json))
    elif filter_text:
        filter_and_conditions.append(Text(filter_text))

    filter_clause = {"$and": filter_and_conditions} if len(filter_and_conditions)>0 else {}

    return await schemas.Session.find(main_query).find(filter_clause).to_list()


async def get_session_by_data_user(data_user: PydanticObjectId, text_filter: str = None, json_filter: str = None,
                                   status: constants.SESSION_STATES = None,
                                   access: constants.SESSION_ACCESS_TYPES = None, time_interval: timedelta = None,
                                   author: str = None, neptune_name: str = None):
    main_query = And(Exists(schemas.Session.data_user, True), schemas.Session.data_user == data_user)
    return await get_session_by_query(main_query=main_query, filter_text=text_filter, filter_json=json_filter,
                                      status=status, access=access, time_interval=time_interval, author=author,
                                      neptune_name=neptune_name)


async def get_session_by_owner(participant: PydanticObjectId, text_filter: str = None, json_filter: str = None,
                                     status: constants.SESSION_STATES = None,
                                     access: constants.SESSION_ACCESS_TYPES = None, time_interval: timedelta = None,
                                     author: str = None, neptune_name: str = None
                                     ):
    main_query = (schemas.Session.data_user == participant)
    return await get_session_by_query(main_query=main_query, filter_text=text_filter, filter_json=json_filter,
                                      status=status, access=access, time_interval=time_interval, author=author,
                                      neptune_name=neptune_name)


async def get_session_by_participant(participant: PydanticObjectId, text_filter: str = None, json_filter: str = None,
                                     status: constants.SESSION_STATES = None,
                                     access: constants.SESSION_ACCESS_TYPES = None, time_interval: timedelta = None,
                                     author: str = None, neptune_name: str = None
                                     ):
    main_query = In(schemas.Session.participants, [participant])
    return await get_session_by_query(main_query=main_query, filter_text=text_filter, filter_json=json_filter,
                                      status=status, access=access, time_interval=time_interval, author=author,
                                      neptune_name=neptune_name)


async def get_session_by_code(code: uuid.UUID):
    return await schemas.Session.find(
        schemas.Session.code == code
    ).first_or_none()


async def get_session(id: PydanticObjectId):
    return await schemas.Session.find(
        schemas.Session.id == id,
    ).first_or_none()


async def get_sessions(text_filter: str = None, json_filter: str = None,
                       status: constants.SESSION_STATES = None,
                       access: constants.SESSION_ACCESS_TYPES = None, time_interval: timedelta = None,
                       author: str = None, neptune_name: str = None
                       ):
    main_query = {}
    return await get_session_by_query(main_query=main_query, filter_text=text_filter, filter_json=json_filter,
                                      status=status, access=access, time_interval=time_interval, author=author,
                                      neptune_name=neptune_name)


async def create_default_session(session: schemas.SessionBase, data_user: PydanticObjectId):
    if session.lifetime > constants.SESSION_MAX_LIFETIME:
        session.lifetime = constants.SESSION_MAX_LIFETIME
    code = uuid.uuid4()
    status = 'active'
    session_create = schemas.SessionCreate(code=code,
                                           data_user=data_user,
                                           events=session.events,
                                           lifetime=session.lifetime,
                                           status=status,
                                           access=session.access,
                                           participants=session.participants,
                                           profiles=session.profiles,
                                           provenance=session.provenance)
    db_session = await create_session(session_create)
    return db_session


async def create_session(session: schemas.SessionCreate):
    db_session = schemas.Session(id=PydanticObjectId(),
                                 code=session.code,
                                 data_user=session.data_user,
                                 events=session.events,
                                 lifetime=session.lifetime,
                                 status=session.status,
				                 access=session.access,
                                 participants=session.participants,
                                 profiles=session.profiles,
                                 provenance=session.provenance,
                                 create_ts=datetime.now(),
                                 modify_ts=datetime.now())
    await db_session.create()
    return db_session


async def update_session(session: schemas.SessionBase, db_session: schemas.Session):
    parameters = session.dict()
    return await update_session_from_dict(parameters=parameters, db_session=db_session)


async def update_session_from_dict(parameters: dict, db_session: schemas.Session):
    req = {k: v for k, v in parameters.items() if v is not None}
    update_query = {"$set": {
        field: value for field, value in req.items()
    }}
    update_query["$set"]["modify_ts"] = datetime.now()
    await db_session.update(update_query)
    return db_session


async def add_session_event(event: schemas.Event, db_session: schemas.Session):
    req = {k: v for k, v in db_session.dict().items() if v is not None}
    req['events'].append(event.dict())
    update_query = {"$set": {"events": req['events'], "modify_ts": datetime.now()}}
    await db_session.update(update_query)
    return db_session


async def update_session_events(events: list[schemas.Event], db_session: schemas.Session):
    update_query = {"$set": {"events": events, "modify_ts": datetime.now()}}
    await db_session.update(update_query)
    return db_session


async def add_session_profile(profile: schemas.Profile, db_session: schemas.Session):
    req = {k: v for k, v in db_session.dict().items() if v is not None}
    for profile_db in req['profiles']:
        if profile_db['tag'] == profile.tag:
            return db_session
    req['profiles'].append(profile.dict())
    update_query = {"$set": {"profiles": req['profiles'], "modify_ts": datetime.now()}}
    await db_session.update(update_query)
    return db_session


async def update_session_profiles(profiles: list[schemas.Profile], db_session: schemas.Session):
    update_query = {"$set": {"profiles": profiles, "modify_ts": datetime.now()}}
    await db_session.update(update_query)
    return db_session


async def add_session_signature(signature_action: ProvenanceActionType, signature_username: str,
                                db_session: schemas.Session, related_session: uuid.UUID = None):
    req = {k: v for k, v in db_session.dict().items() if v is not None}
    signature = signature_action + '::' + signature_username + '::' + str(datetime.now())
    if related_session:
        signature = signature + '::' + str(related_session)
    req['provenance'].append(signature)
    update_query = {"$set": {"provenance": req['provenance'], "modify_ts": datetime.now()}}
    await db_session.update(update_query)
    return db_session


async def update_session_provenance(provenance: list[str], db_session: schemas.Session):
    update_query = {"$set": {"provenance": provenance, "modify_ts": datetime.now()}}
    await db_session.update(update_query)
    return db_session


async def delete_session(id: PydanticObjectId):
    await schemas.Session.find(
        schemas.Session.id == id,
    ).delete_many()


async def delete_session_by_data_user(data_user: PydanticObjectId):
    await schemas.Session.find(
        schemas.Session.data_user == data_user,
    ).delete_many()


async def delete_all_sessions():
    await schemas.Session.delete_all()
