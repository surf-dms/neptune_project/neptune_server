from fastapi import APIRouter, Depends, HTTPException, Query, status, Request
from typing import List, Annotated
from beanie import PydanticObjectId

from .. import schemas, crud, roles, helpers, constants


router = APIRouter(
    prefix="/data",
    tags=["data"],
    responses={404: {"description": "Not found"}},
)


@router.post("/", dependencies=[Depends(roles.RoleChecker(["admin","user","provider"]))])
async def create_data(data: schemas.DataCreate) -> schemas.Data:
    db_data = await crud.get_data_by_provider(data.key_in_provider, data.provider_id)
    if db_data:
        raise HTTPException(status_code=400, detail="Data already registered")
    return await crud.create_data(data=data)


@router.get("/", dependencies=[Depends(roles.RoleChecker(["admin","user","provider"]))])
async def list_data(email: str | None = None,
                    key_in_provider: str | None = None,
                    provider_id: PydanticObjectId | None = None,
                    usernames: Annotated[list[PydanticObjectId] | None, Query()] = None) -> List[schemas.Data]:
    if key_in_provider and provider_id:
        return await crud.get_data_by_provider(provider_location=key_in_provider, provider_id=provider_id)
    if email:
        return await crud.get_data_by_email(email=email)
    elif usernames:
        if provider_id:
            return await crud.get_data_by_provider_username(usernames=usernames, provider_id=provider_id)
        else:
            return await crud.get_data_by_username(usernames=usernames)
    elif provider_id:
        return await crud.get_all_data_by_provider(provider_id=provider_id)
    return await crud.get_data()


@router.patch("/{id}", dependencies=[Depends(roles.RoleChecker(["admin","user","provider"]))])
async def update_data(id: PydanticObjectId, data: schemas.DataCreate) -> schemas.Data:
    db_data = await crud.update_data(id=id, data=data)
    return db_data


@router.delete("/", dependencies=[Depends(roles.RoleChecker(["admin","user","provider"]))])
async def delete_data(id: PydanticObjectId | None = None) -> dict:
    if id:
        await crud.delete_data(data_id=id)
    else:
        await crud.delete_all_data()
    return {
        "message": "Data deleted successfully"
    }