from fastapi import APIRouter, Depends, HTTPException, Query, status, Request
from typing import List
from beanie import PydanticObjectId

from .. import schemas, crud, roles, helpers, constants


router = APIRouter(
    prefix="/data_users",
    tags=["data_users"],
    responses={404: {"description": "Not found"}},
)


@router.post("/", dependencies=[Depends(roles.RoleChecker(["admin"]))])
async def create_data_user(data_user: schemas.DataUserCreate):
    db_data_user = await crud.get_data_user_by_provider_username(username=data_user.username,
                                                                 provider_id=data_user.provider_id)
    if db_data_user:
        raise HTTPException(status_code=400, detail="Data user already registered")
    return await crud.create_data_user(data_user=data_user)


@router.get("/", dependencies=[Depends(roles.RoleChecker(["admin","user","provider","reviewer"]))])
async def list_data_users(request: Request,
                          provider_id: PydanticObjectId | None = None,
                          username: str | None = None,
                          id: PydanticObjectId | None = None) -> List[schemas.DataUser]:
    data_user = request.state.data_user_db
    result = []
    if id:
        response = await crud.get_data_user(user_id=id)
        if response:
            result = [response]
        else:
            result = []
    if provider_id and ((data_user.role == "provider") or data_user.role == "admin"):
        result = await crud.get_data_user_by_provider_id(provider_id=provider_id)
    if username:
        if provider_id:
            response = await crud.get_data_user_by_provider_username(username=username, provider_id=provider_id)
            if response:
                result = [response]
            else:
                result = []
        elif data_user.role == "admin":
            result = await crud.get_data_user_by_username(username=username)
    if len(result) == 0 and not(id or provider_id or username) and data_user.role == "admin":
        result = await crud.get_data_users()
    if result is None:
        result = []

    return result


@router.patch("/{id}", dependencies=[Depends(roles.RoleChecker(["admin"]))])
async def update_data_user(id: PydanticObjectId, data_user: schemas.DataUserCreate) -> schemas.DataUser:
    db_data_user = await crud.update_data_user(data_user_id=id, data_user=data_user)
    return db_data_user


@router.delete("/", dependencies=[Depends(roles.RoleChecker(["admin"]))])
async def delete_data_user(id: PydanticObjectId | None = None,
                           provider_id: PydanticObjectId | None = None) -> dict:
    if id:
        await crud.delete_data_user(data_user_id=id)
    else:
        await crud.delete_data_users(provider_id=provider_id)
    return {
        "message": "Data users deleted successfully"
    }