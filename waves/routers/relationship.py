from fastapi import APIRouter, Depends, HTTPException, Query, status, Request
from typing import List, Annotated
from beanie import PydanticObjectId

from .. import schemas, crud, roles, helpers, constants


router = APIRouter(
    prefix="/relationship",
    tags=["relationship"],
    responses={404: {"description": "Not found"}},
)


@router.post("/", dependencies=[Depends(roles.RoleChecker(["admin"]))])
async def create_relationship(relationship: schemas.RelationshipCreate) -> schemas.Relationship:
    db_relationship = await crud.get_relationship(relationship.data, relationship.data_user)
    if db_relationship:
        raise HTTPException(status_code=400, detail="Relationship already registered")
    return await crud.create_relationship(relationship=relationship)


@router.get("/", dependencies=[Depends(roles.RoleChecker(["admin", "user", "provider"]))])
async def list_relationship(data_id: PydanticObjectId | None = None,
                            data_user: PydanticObjectId | None = None) -> List[schemas.Relationship]:
    if data_user:
        if data_id:
            return await crud.get_relationship(data=data_id, data_user=data_user)
        else:
            return await crud.get_relationship_by_data_user(data_user=data_user)
    elif data_id:
        return await crud.get_relationship_by_data(data=data_id)
    return await crud.get_relationships()


@router.delete("/", dependencies=[Depends(roles.RoleChecker(["admin"]))])
async def delete_relationship(id: PydanticObjectId | None = None) -> dict:
    if id:
        await crud.delete_relationship(id=id)
    else:
        await crud.delete_all_relationships()
    return {
        "message": "Relationship deleted successfully"
    }


@router.patch("/{id}", dependencies=[Depends(roles.RoleChecker(["admin"]))])
async def update_relationship(id: PydanticObjectId, relationship: schemas.RelationshipCreate) -> schemas.Relationship:
    db_relationship = await crud.update_relationship(id=id, relationship=relationship)
    return db_relationship