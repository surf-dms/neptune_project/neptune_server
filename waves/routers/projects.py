from fastapi import APIRouter, Depends, HTTPException, Query, status, Request
from typing import List
from beanie import PydanticObjectId

from .. import schemas, crud, roles, helpers, constants


router = APIRouter(
    prefix="/projects",
    tags=["projects"],
)


@router.post("/", dependencies=[Depends(roles.RoleChecker(["admin"]))])
async def create_project(project: schemas.ProjectCreate) -> schemas.Project:
    proj = await schemas.Project.find(schemas.Project.name == project.name).first_or_none()
    if not proj:
        db_project = await crud.create_project(project)
    else:
        raise HTTPException(status_code=400, detail="Project already registered")
    return db_project


@router.patch("/{id}", dependencies=[Depends(roles.RoleChecker(["admin"]))])
async def update_project(id: PydanticObjectId, project: schemas.ProjectCreate) -> schemas.Project:
    db_project = await crud.update_project(project_id=id, project=project)
    return db_project


@router.get("/", dependencies=[Depends(roles.RoleChecker(["admin","user","provider","reviewer"]))])
async def list_projects(request: Request,
                        name: str | None = None) -> List[schemas.Project]:
    data_user = request.state.data_user_db
    result = None
    if name:
        project = await crud.get_project_by_name(name=name)
        if project and (data_user.id in project.members or data_user.role == "admin"):
            result = [project]
        else:
            result = []
    else:
        if data_user.role in ["user", "provider", "reviewer"]:
            return await crud.get_project_by_participant(member=data_user.id)
        elif data_user.role == "admin":
            return await crud.get_projects()
        else:
            return []

    return result


@router.delete("/", dependencies=[Depends(roles.RoleChecker(["admin"]))])
async def delete_projects(id: PydanticObjectId | None = None) -> dict:
    if id:
        await crud.delete_project(id)
    else:
        await crud.delete_projects()
    return {
        "message": "Projects deleted successfully"
    }


@router.post("/{name}/sram/{email}", dependencies=[Depends(roles.RoleChecker(["admin","provider"]))])
async def register_project_sram_user(name: str, email: str) -> bool:
    sram_user = await crud.verify_project_sram_user(project_name=name, email=email)
    if not sram_user:
        new_sram_user = await crud.register_project_sram_user(project_name=name, email=email)
    else:
        raise HTTPException(status_code=400, detail="user already registered in SRAM")
    return new_sram_user


@router.get("/{name}/sram/{email}", dependencies=[Depends(roles.RoleChecker(["admin","provider"]))])
async def verify_project_sram_user_registration(name: str, email: str) -> bool:
    return await crud.verify_project_sram_user(project_name=name, email=email)


@router.delete("/{name}/sram/{email}", dependencies=[Depends(roles.RoleChecker(["admin","provider"]))])
async def delete_project_sram_user(name: str, email: str) -> bool:
    return await crud.delete_project_sram_user(project_name=name, email=email)