from fastapi import APIRouter, Depends, HTTPException, Query, status, Request
from typing import List, Annotated
from beanie import PydanticObjectId

from .. import schemas, crud, roles, helpers, constants


router = APIRouter(
    prefix="/metadata",
    tags=["metadata"],
    responses={404: {"description": "Not found"}},
)


@router.post("/", dependencies=[Depends(roles.RoleChecker(["admin", "user", "provider"]))])
async def create_metadata(request: Request,
                          metadata: schemas.MetaDataBase) -> schemas.MetaData:
    data_user = request.state.data_user_db
    unique_name = helpers.get_metadata_unique_name(content=metadata.content,
                                                   meta_id=metadata.provider_md_id,
                                                   provider_id=metadata.provider_id)
    db_metadata = await crud.get_metadata_by_unique_name(unique_name)
    if db_metadata:
        raise HTTPException(status_code=400, detail="MetaData already registered")
    return await crud.create_metadata(metadata=metadata, data_users=[data_user])


@router.get("/", dependencies=[Depends(roles.RoleChecker(["admin", "user", "provider"]))])
async def list_metadata(request: Request,
                        unique_name: str | None = None,
                        provider_md_id: str | None = None,
                        provider_id: PydanticObjectId | None = None,
                        data_users: Annotated[list[PydanticObjectId] | None, Query()] = None) -> List[schemas.MetaData]:
    data_user = request.state.data_user_db
    metadata_db_list = []
    if unique_name:
        metadata_db = await crud.get_metadata_by_unique_name(unique_name=unique_name)
        if metadata_db:
            metadata_db_list.append(metadata_db)
    elif provider_md_id:
        if provider_id:
            metadata_db = await crud.get_metadata_by_provider_id_md_id(provider_md_id=provider_md_id,
                                                                       provider_id=provider_id)
        else:
            metadata_db = await crud.get_metadata_by_provider_md_id(provider_md_id=provider_md_id)
        if metadata_db:
            metadata_db_list.append(metadata_db)
    elif provider_id:
        if data_users:
            metadata_db_list = await crud.get_metadata_by_provider_username(provider_id=provider_id,
                                                                            usernames=data_users)
        else:
            metadata_db_list = await crud.get_all_metadata_by_provider(provider_id=provider_id)
    else:
        metadata_db_list = await crud.get_metadata()
    accessible_metadata_db_list = []
    for metadata_db in metadata_db_list:
        intersessions = []
        if metadata_db.sessions:
            sessions_db = crud.get_session_by_participant(data_user.id)
            intersessions = [session for session in sessions_db if session.id in metadata_db.sessions]
        if (metadata_db.access == 'public' or (data_user.id in metadata_db.data_users)
                or len(intersessions) > 0 or (data_user.role == "admin")):
            accessible_metadata_db_list.append(metadata_db)

    return accessible_metadata_db_list


@router.patch("/{id}", dependencies=[Depends(roles.RoleChecker(["admin", "user", "provider"]))])
async def update_metadata(request: Request,
                          id: PydanticObjectId, metadata: schemas.MetaDataBase) -> schemas.MetaData:
    data_user = request.state.data_user_db
    if (data_user.id in metadata.data_users) or data_user.role == "admin":
        return await crud.update_metadata(id=id, metadata=metadata)
    else:
        return None


@router.delete("/", dependencies=[Depends(roles.RoleChecker(["admin", "user", "provider"]))])
async def delete_metadata(request: Request,
                          id: PydanticObjectId | None = None) -> dict:
    data_user = request.state.data_user_db
    metadata_db = await crud.get_metadata_by_id(metadata_id=id)
    if id and ((data_user.id in metadata_db.data_users) or data_user.role == "admin"):
        await crud.delete_metadata(metadata_id=id)
    elif data_user.role == "admin":
        await crud.delete_all_metadata()
    else:
        return {
            "message": "Insufficient permissions"
        }
    return {
        "message": "Data deleted successfully"
    }
