import uuid
import datetime

from fastapi import APIRouter, Depends, HTTPException, Query, status, Request
from typing import List, Annotated
from beanie import PydanticObjectId

from .. import schemas, crud, roles, helpers, constants
from ..config import config_dict

router = APIRouter(
    prefix="/session",
    tags=["session"],
    responses={404: {"description": "Not found"}},
)


@router.post("/", dependencies=[Depends(roles.RoleChecker(["admin", "user", "provider"]))])
async def create_session(request: Request,
                         session: schemas.SessionBase) -> schemas.Session:
    data_user = request.state.data_user_db
    return await crud.create_default_session(session, data_user.id)


@router.get("/", dependencies=[Depends(roles.RoleChecker(["admin", "user", "provider", "reviewer"]))])
async def list_session(request: Request,
                       code: uuid.UUID | None = None,
                       status: constants.SESSION_STATES | None = 'active',
                       access: constants.SESSION_ACCESS_TYPES | None = None,
                       time_interval: datetime.timedelta | None = None,
                       author: str | None = None,
                       text: str | None = None,
                       json_search: str | None = None) -> List[schemas.Session]:
    data_user = request.state.data_user_db
    if code:
        session = await crud.get_session_by_code(code=code)
        if (session and
                (session.data_user == data_user.id or data_user.role == "admin" or
                 ((session.access == 'public' or data_user.id in session.participants) and
                  session.status == 'active'))):
            return [session]
        else:
            return []
    else:
        #neptune_name = Settings().neptune_server_friendly_name
        neptune_name = config_dict['default']['neptune_server_friendly_name']
        if data_user.role in ["user", "provider", "reviewer"]:
            sessions_as_owner = await crud.get_session_by_owner(participant=data_user.id, text_filter=text,
                                                                json_filter=json_search,
                                                                status=status, access=access,
                                                                time_interval=time_interval,
                                                                author=author, neptune_name=neptune_name
                                                                )
            sessions_as_participant = await crud.get_session_by_participant(participant=data_user.id, text_filter=text,
                                                                            json_filter=json_search,
                                                                            status='active', access=access,
                                                                            time_interval=time_interval,
                                                                            author=author, neptune_name=neptune_name
                                                                            )
            return sessions_as_owner + sessions_as_participant
        elif data_user.role == "admin":
            return await crud.get_sessions(text_filter=text, json_filter=json_search,
                                           status=status, access=access, time_interval=time_interval,
                                           author=author, neptune_name=neptune_name
                                           )
        else:
            return []


@router.patch("/{id}", dependencies=[Depends(roles.RoleChecker(["admin", "user", "provider"]))])
async def update_session(request: Request, id: PydanticObjectId, session: schemas.SessionBase) -> schemas.Session:
    data_user = request.state.data_user_db
    db_session = await crud.get_session(id=id)
    if not db_session:
        raise HTTPException(status_code=404, detail="Session not found!")
    if (db_session.data_user == data_user.id) or (data_user.role == "admin"):
        db_session = await crud.update_session(session=session, db_session=db_session)
    return db_session


@router.post("/{id}/events", dependencies=[Depends(roles.RoleChecker(["admin", "user", "provider"]))])
async def add_session_event(request: Request, id: PydanticObjectId, event: schemas.Event) -> schemas.Session:
    data_user = request.state.data_user_db
    db_session = await crud.get_session(id=id)
    if not db_session:
        raise HTTPException(status_code=404, detail="Session not found!")
    if (db_session.data_user == data_user.id) or (data_user.role == "admin"):
        db_session = await crud.add_session_event(event=event, db_session=db_session)
    return db_session


@router.patch("/{id}/events", dependencies=[Depends(roles.RoleChecker(["admin", "user", "provider", "reviewer"]))])
async def update_session_events(request: Request, id: PydanticObjectId, events: list[schemas.Event]) -> schemas.Session:
    data_user = request.state.data_user_db
    db_session = await crud.get_session(id=id)
    if not db_session:
        raise HTTPException(status_code=404, detail="Session not found!")
    if ((db_session.data_user == data_user.id) or (data_user.role == "admin")
            or (data_user.id in db_session.participants and data_user.role == "reviewer")):
        db_session = await crud.update_session_events(events=events, db_session=db_session)
    else:
        raise HTTPException(status_code=403, detail="Operation 'update session event' not " +
                                                    "allowed for user {}".format(data_user.username))
    return db_session


@router.post("/{id}/profiles",
          dependencies=[Depends(roles.RoleChecker(["admin", "user", "provider", "reviewer"]))])
async def add_session_profile(request: Request, id: PydanticObjectId, profile: schemas.Profile) -> schemas.Session:
    data_user = request.state.data_user_db
    db_session = await crud.get_session(id=id)
    if not db_session:
        raise HTTPException(status_code=404, detail="Session not found!")
    if ((db_session.data_user == data_user.id) or (data_user.role == "admin")
            or (data_user.id in db_session.participants and data_user.role == "reviewer")):
        db_session = await crud.add_session_profile(profile=profile, db_session=db_session)
    return db_session


@router.patch("/{id}/profiles",
           dependencies=[Depends(roles.RoleChecker(["admin", "user", "provider", "reviewer"]))])
async def update_session_profile(request: Request,
                                 id: PydanticObjectId,
                                 profiles: list[schemas.Profile]) -> schemas.Session:
    data_user = request.state.data_user_db
    db_session = await crud.get_session(id=id)
    if not db_session:
        raise HTTPException(status_code=404, detail="Session not found!")
    if ((db_session.data_user == data_user.id) or (data_user.role == "admin")
            or (data_user.id in db_session.participants and data_user.role == "reviewer")):
        db_session = await crud.update_session_profiles(profiles=profiles, db_session=db_session)
    return db_session


@router.post("/{id}/provenance",
          dependencies=[Depends(roles.RoleChecker(["admin", "user", "provider", "reviewer"]))])
async def add_session_signature(request: Request,
                                id: PydanticObjectId, action: crud.ProvenanceActionType,
                                related_session: uuid.UUID | None = None) -> schemas.Session:
    data_user = request.state.data_user_db
    db_session = await crud.get_session(id=id)
    if not db_session:
        raise HTTPException(status_code=404, detail="Session not found!")
    if ((db_session.data_user == data_user.id)
            or ((data_user.role in ["user", "provider", "reviewer"]) and (data_user.id in db_session.participants))
            or data_user.role == "admin"):
        if ((data_user.role == "reviewer" and action == crud.ProvenanceActionType.copied)
                or (data_user.role == "user" and action != crud.ProvenanceActionType.copied)):
            raise HTTPException(status_code=403, detail="User {} with role {} cannot register action '{}'".format(
                data_user.username, data_user.role, action))
        db_session = await crud.add_session_signature(signature_action=action, signature_username=data_user.username,
                                                      db_session=db_session, related_session=related_session)
    else:
        raise HTTPException(status_code=403, detail="Operation 'add session signature' not " +
                                                    "allowed for user {}".format(data_user.username))
    return db_session


@router.patch("/{id}/provenance", dependencies=[Depends(roles.RoleChecker(["admin", "user", "provider"]))])
async def update_session_provenance(request: Request, id: PydanticObjectId, provenance: list[str]) -> schemas.Session:
    data_user = request.state.data_user_db
    db_session = await crud.get_session(id=id)
    if not db_session:
        raise HTTPException(status_code=404, detail="Session not found!")
    if (db_session.data_user == data_user.id) or (data_user.role == "admin"):
        db_session = await crud.update_session_provenance(provenance=provenance, db_session=db_session)
    else:
        raise HTTPException(status_code=403, detail="Operation 'update session provenance' not " +
                                                    "allowed for user {}".format(data_user.username))
    return db_session


@router.delete("/", dependencies=[Depends(roles.RoleChecker(["admin", "user", "provider"]))])
async def delete_session(request: Request, id: PydanticObjectId | None = None) -> dict:
    data_user = request.state.data_user_db
    if id:
        session = await crud.get_session(id=id)
        if session and ((session.data_user == data_user.id) or (data_user.role == "admin")):
            await crud.delete_session(id=id)
            msg = "Session deleted successfully"
        else:
            msg = "Session not found"
    else:
        if (data_user.role == "user") or (data_user.role == "provider"):
            await crud.delete_session_by_data_user(data_user=data_user.id)
            msg = "Session(s) deleted successfully"
        elif data_user.role == "admin":
            await crud.delete_all_sessions()
            msg = "Session(s) deleted successfully"
        else:
            msg = "Nothing to delete"
    return {"message": msg}