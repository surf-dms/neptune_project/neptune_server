from fastapi import APIRouter, Depends, HTTPException, Query, status, Request
from typing import List
from beanie import PydanticObjectId

from .. import schemas, crud, roles, helpers, constants


router = APIRouter(
    prefix="/providers",
    tags=["providers"],
    responses={404: {"description": "Not found"}},
)


@router.post("/", dependencies=[Depends(roles.RoleChecker(["admin"]))])
async def create_provider(provider: schemas.ProviderCreate) -> schemas.Provider:
    prov = await crud.get_provider_by_endpoint(provider.endpoints)
    if not prov:
        db_provider = await crud.create_provider(provider)
    else:
        raise HTTPException(status_code=400, detail="Endpoint already registered")
    return db_provider


@router.get("/", dependencies=[Depends(roles.RoleChecker(["admin","user","provider","reviewer"]))])
async def list_providers(hostname: str | None = None,
                         port: int | None = None,
                         protocol: str | None = None,
                         ep_type: str | None = None,
                         friendly_name: str | None = None) -> List[schemas.Provider]:
    result = None
    if hostname:
        result = await crud.get_providers_by_hostname(hostname=hostname)
    if port:
        if result:
            result_port = await crud.get_providers_by_port(port=port)
            result = result + [data for data in result_port if data not in result]
        else:
            result = await crud.get_providers_by_port(port=port)
    if protocol:
        if result:
            result_protocol = await crud.get_providers_by_protocol(protocol=protocol)
            result = list(set(result_protocol) & set(result))
        else:
            result = await crud.get_providers_by_port(port=port)
    if ep_type:
        if result:
            result_type = await crud.get_providers_by_type(ep_type=ep_type)
            result = list(set(result_type) & set(result))
        else:
            result = await crud.get_providers_by_type(ep_type=ep_type)
    if friendly_name:
        if result:
            result_friendly_name = await crud.get_providers_by_name(name=friendly_name)
            result = list(set(result_friendly_name) & set(result))
        else:
            result = await crud.get_providers_by_name(name=friendly_name)
    if result is None:
        result = await crud.get_providers()

    return result


@router.delete("/", dependencies=[Depends(roles.RoleChecker(["admin"]))])
async def delete_providers(id: PydanticObjectId | None = None) -> dict:
    if id:
        await crud.delete_provider(id)
    else:
        await crud.delete_providers()
    return {
        "message": "Providers deleted successfully"
    }