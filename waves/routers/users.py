from fastapi import APIRouter, Depends, HTTPException, Query, status, Request
from typing import List
from beanie import PydanticObjectId

from .. import schemas, crud, roles, helpers, constants


router = APIRouter(
    prefix="/users",
    tags=["users"],
    responses={404: {"description": "Not found"}},
)


@router.get("/me", dependencies=[Depends(roles.RoleChecker(["admin","user","provider","reviewer"]))])
def read_current_user(request: Request) -> schemas.DataUser:
    return request.state.data_user_db


@router.post("/", dependencies=[Depends(roles.RoleChecker(["admin"]))])
async def create_user(user: schemas.UserCreate) -> schemas.User:
    usr = await schemas.User.find(
        schemas.User.surf_id == user.surf_id,
    ).first_or_none()
    if not usr:
        db_user = await crud.create_user(user)
    else:
        raise HTTPException(status_code=400, detail="User already registered")
    return db_user


@router.get("/", dependencies=[Depends(roles.RoleChecker(["admin"]))])
async def list_users(surf_id: str | None = None, email: str | None = None) -> List[schemas.User]:
    result = None
    if surf_id:
        result = [await crud.get_user_by_surf_id(surf_id=surf_id)]
    if email:
        if surf_id:
            pass
        else:
            result = await crud.get_user_by_email(email=email)
    if not result:
        result = await crud.get_users()

    return result


@router.patch("/{id}", dependencies=[Depends(roles.RoleChecker(["admin"]))])
async def update_user(id: PydanticObjectId, user: schemas.UserCreate) -> schemas.User:
    db_user = await crud.update_user(user_id=id, user=user)
    return db_user


@router.delete("/", dependencies=[Depends(roles.RoleChecker(["admin"]))])
async def delete_users(id: PydanticObjectId | None = None) -> dict:
    if id:
        await crud.delete_user(id)
    else:
        await crud.delete_users()
    return {
        "message": "Users deleted successfully"
    }