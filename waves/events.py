import datetime
import asyncio
from pydantic_settings import BaseSettings
from apscheduler.schedulers.asyncio import AsyncIOScheduler
from apscheduler.schedulers.background import BackgroundScheduler
from apscheduler.triggers.cron import CronTrigger
from apscheduler.triggers.interval import IntervalTrigger
from . import crud, schemas


scheduler = AsyncIOScheduler()


async def session_lifetime_manager():
    json_search = '{"status": "active"}'
    sessions = await crud.get_sessions(json_filter=json_search)
    for session in sessions:
        if (datetime.datetime.now() - session.create_ts) > datetime.timedelta(seconds=int(session.lifetime)):
            await crud.update_session_from_dict(parameters={"status": "expired"}, db_session=session)


#async def init_scheduler(settings: BaseSettings):
async def init_scheduler(config: dict):
    # Set up the scheduler
    #trigger = IntervalTrigger(seconds=settings.neptune_scheduler_session_lcti)
    trigger = IntervalTrigger(seconds=int(config['neptune_scheduler_session_lcti']))
    scheduler.add_job(session_lifetime_manager, trigger)

