import hashlib
import configparser
import os
import os.path

from . import constants


def get_metadata_unique_name(content: None, meta_id: None, provider_id: None):

    if content:
        unique_name = (hashlib.md5(content.encode())).hexdigest()
    elif meta_id:
        if provider_id:
            coordinates = str(provider_id) + meta_id
            unique_name = (hashlib.md5(coordinates.encode())).hexdigest()
        else:
            unique_name = (hashlib.md5(meta_id.encode())).hexdigest()

    return unique_name


def config_section_map(config, section):
    dict1 = {}
    options = config.options(section)
    for option in options:
        dict1[option] = config.get(section, option)
    return dict1


def parse_configuration(conf=constants.SERVER_CONFIG_PATH):
    conf_dict = {}
    if os.path.exists(conf):
        config = configparser.ConfigParser()
        config.read(conf)
        conf_dict["default"] = config.defaults()
        for section in config.sections():
            conf_dict[section] = config_section_map(config, section)
        return None, conf_dict
    else:
        return "The configuration file {} doesn't exist".format(conf), conf_dict


