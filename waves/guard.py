import os
import secrets
from cryptography import fernet
from passlib.context import CryptContext


pwd_context=CryptContext(schemes=["bcrypt"], deprecated="auto")


def hash_bcrypt(password: str):
    return pwd_context.hash(password)


def hash_verify(hashed_password, plain_password):
    return pwd_context.verify(plain_password, hashed_password)


def decrypt_secret(secret: str, key: str):
    cipher = fernet.Fernet(bytes.fromhex(key))
    try:
        return cipher.decrypt(bytes.fromhex(secret))
    except fernet.InvalidToken:
        raise Exception("invalid encryption key")


def encrypt_secret(secret: bytes, key: bytes = None):
    if not key:
        key = fernet.Fernet.generate_key()
    cipher = fernet.Fernet(key)
    return {"secret": cipher.encrypt(secret),
            "key": key}

