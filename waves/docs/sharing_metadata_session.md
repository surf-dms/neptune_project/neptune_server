 ```json
 {
    "access": "shared",
    "events": [
        {
            "metadata": [
                [
                    "system",
                    "surf-yoda::/yoda/home/myproject/subdir0",
                    {
                        "checksum": {
                            "/yoda/home/myproject/subdir0/hello_yoda.txt": {
                                "sum": "e67a70d6d4275a85821dd7a4449dfc130f5a3cfe96bdc4ac0886780ddfe37b81",
                                "type": "sha256"
                            },
                            "/yoda/home/myproject/subdir0/subdir1/hello_irods.txt": {
                                "sum": "00fbb1007de1fc4a5a28c4a88103046dd6e2bb762ec769f9efbac5de8c2b9fe3",
                                "type": "sha256"
                            }
                        },
                        "size": 25,
                        "type": "collection"
                    }
                ],
                [
                    "",
                    "",
                    {
                        "text": "just metadata"
                    }
                ]
            ],
            "operation": "share",
            "path": "/yoda/home/myproject/subdir0",
            "profile_tags": {
                "path": "surf-yoda.myhost.nl:1247::anonymous::native"
            }
        }
    ],
    "lifetime": 168,
    "participants": [
        "66ebe73b857c0122597ab096"
    ],
    "profiles": [
        {
            "auth_scheme": "native",
            "endpoint": {
                "ep_type": "irods",
                "hostname": "surf-yoda.myhost.nl",
                "port": 1247,
                "protocol": "irods",
                "zone": "yoda"
            },
            "metadata": {},
            "secret_type": "ticket",
            "tag": "surf-yoda.myhost.nl:1247::anonymous::native",
            "username": "anonymous"
        }
    ]
}
```