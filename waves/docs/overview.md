# Introduction

Neptune is built around the data or, better, around a metadata model that represents an infrastructure, according to 
this [idea](modeling_the_infrastructure.md).  

A data flow is a representation of how data "flows" within one system or through multiple systems. Each flow has 
multiple steps. Each step is an operation performed on data, such as a copy, a replication, a download, a query and so on.  
Sometimes, the operation affects only the metadata and not the related data.

An initial configuration is described [here](configuration.md).  

This is an example of a data flow supported by Neptune:

[shopping cart data flow](shopping_cart_flow/data_flow-shopping_cart.md)  

This is an example of authentication flow supported by Neptune:  

[authentication flow: SRAM by invitation](auth_flow-sram-by-invitation.md)

Both the above examples require that a project is defined.  
A Project, in the Neptune terminology, is an entity that links together users and providers.  
One special provider in a project is 'SRAM' ([SURF Research Access Management](https://sram.surf.nl/)).  
Each project may be mapped to one and only one SRAM Collaboration (see documentation on the SRAM web site).  
This is an example of project:

[project example](project.md)


