# Data at the center
We often see the data as objects stored, copied, downloaded ... We have tools to handle data and infrastructures to support
such tools. Metadata are mostly understood as appendages of data and handled with the same or similar tools.  
We would like to explore the idea to give a more central place to the data.  
For example, we could think of the tools, the services, and the related infrastructures as bidimensional spaces, where we
project the data, which we see as tridimensional objects belonging to a different space.

![Data objects as 3D objects](img/data3d.png "Data objects as 3D objects")

The data stored on those infrastructures and handled by those services are described within the service space by a set of
metadata. We believe that it is possible to model those metadata to make the data description interoperable with different
systems. In turns the systems are also described through metadata, which can be modeled so that it is possible to apply 
the same operations to different systems.
Re-using the metaphor of the two-dimensional space, we could say that the projection of the data can be modelled always 
with the same geometrical form, for example a square, and the same is valid for the systems.

![infrastructure and services as 2D objects](img/infra_metadata.jpg "infrastructure and services as 2D objects")

According to this idea, Neptune is the space of those metadata models.  
The definitions of those models for different systems would allow to make them interoperable.  
The interoperability would then be implemented not through one-to-one API connections, but through a all-to-Neptune-to-all
space of connections. Where this space is logical, not necessary physical. For performance or scalability reasons, you may
want that two systems are directly connected. Only the logic making that connection happening would not be within each system,
but delegated to Neptune.  
The same models would allow to automate operations on multiple systems providing a solid base for 
the implementation of workflows.

### Outer space

This idea is ambitious.  
Is it too ambitious? Does every existing system and service has to be described and modeled within Neptune to be connected 
to the rest of the infrastructures?   
That is not the intention. Once the description of a generic data object and a generic data service have been modeled, 
then those descriptions can be used to "wrap" the data object in a capsule that can travel boldly in the outer space of 
unknown systems. Again, the idea is to provide a minimal set of attributes that can be universally understood. For example,
it is possible to create a package that contains data and metadata and provide a library that is able to un-package it.
Any system in the outer space could try to use the library to un-wrap the package and see if it can interpret the content.

### Inner space

What about the rest of the metadata related to a data object? It is a world per se. They include, for example, descriptive
metadata about the people and the organizations that created or simply handled those data. Sensors related metadata, provenance,
legal and infinite layers of domain specific metadata.
They all have citizenship within the Neptune space, but Neptune will never model them. Maybe only a tiny sub-set. However,
there are projects that aims to describe those. Like Neptune is a meta-metadata framework for infrastructures, those projects
are for the inner space of specific metadata.  
Neptune challenge is learning how to reference them and link the systems to them.  