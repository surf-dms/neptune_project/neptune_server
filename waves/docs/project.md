# Project

You can create and configure a new project in this way:  

1. create a project:
```commandline
curl -X 'POST' \
  'http://localhost:8080/projects/' \
  -H 'accept: application/json' \
  -H 'Authorization: Basic c4VyrmFkbWduOm1pbWRhZnJ5cw==' \
  -H 'Content-Type: application/json' \
  -d '{
  "name": "Triton",
  "description": "a new project",
  "expiration_ts": "2024-09-20T11:59:38.324Z"
}'
```
2. add the mapping between service accounts and providers  
A 'service account' is a Neptune's data user with role 'provider'. it is expected to be a type of account used to automate tasks which are performed by data providers' administrators.  
One and only one service account is expected to be used by a data provider within a specific project.  
```commandline
curl -X 'PATCH' \
  'http://localhost:8080/project/66ec12ca857c0122597ab0a2' \
  -H 'accept: application/json' \
  -H 'Authorization: Basic c3VyZmFkbWluOm5pbWRhZnJ1cw==' \
  -H 'Content-Type: application/json' \
  -d '{
  "name": "Triton",
  "description": "a new project",
  "expiration_ts": "2024-09-20T12:10:35.563Z",
  "service_accounts": {
      "6613bc69f7685abdfbc33a4f": "6627d9241768071da0c4edee",
      "6633aeac8accd706fef14fa6": "6627d7111768071da0c4edec"
  }
}'
```
Notice that the value of the field 'service_accounts' is a dictionary, whose keys are data users' ids and the values are providers' ids.  
How do you get those ids?  
You can list the data users:  
```commandline
curl -X 'GET' \
  'http://localhost:8080/data_users/' \
  -H 'accept: application/json' \
  -H 'Authorization: Basic c4VyZmFkbWluOm5cdrWRhZnJ1cw=='
```
And you can list the providers:
```commandline
curl -X 'GET' \
  'http://localhost:8080/providers/' \
  -H 'accept: application/json' \
  -H 'Authorization: Basic c0VyZmFkbWghOm4pbWRh1ZnJ1cw=='
```
3. couple the project with a SRAM collaboration
```commandline
curl -X 'PATCH' \
  'http://localhost:8080/project/66ec12ca857c0122597ab0a2' \
  -H 'accept: application/json' \
  -H 'Authorization: Basic c3VwZmFkbrTuOm5pbWRvZnJ9cw==' \
  -H 'Content-Type: application/json' \
  -d '{
    "description": "a new project",
    "expiration_ts": "2024-09-20 12:10:35.563000",
    "name": "Triton",
    "service_accounts": {
        "6613bc69f7685abdfbc33a4f": "6627d9241768071da0c4edee",
        "6633aeac8accd706fef14fa6": "6627d7111768071da0c4edec"
    },
    "sram_api_key": "BWHAUGt14JdcMmAoNX-WmPB7AnoXtOYaoR1CdS57oCEOq",
    "sram_enabled": true
}'
```