```json
  {
    "access": "shared",
    "events": [
        {
            "metadata": [],
            "operation": "request",
            "path": "/yoda/home/myproject/subdir0",
            "profile_tags": {
                "path": "surf-yoda.myhost.nl:1247::noaccess::native",
                "user": "sram.surf.nl:443::Guybrush.Threepwood::native"
            }
        }
    ],
    "lifetime": 168,
    "participants": [
        "66ebe750857c0122597ab09c",
        "66ebe74f857c0122597ab09a"
    ],
    "profiles": [
        {
            "auth_scheme": "native",
            "endpoint": {
                "ep_type": "web portal",
                "hostname": "sram.surf.nl",
                "port": 443,
                "protocol": "https"
            },
            "metadata": {
                "user": {
                    "email": "guybrush.threepwood@monkeyisland.pirates",
                    "organization": "rogue",
                    "project": "treasure hunt",
                    "role": "pirate candidate"
                }
            },
            "secret_type": "password",
            "tag": "sram.surf.nl:443::Guybrush.Threepwood::native",
            "username": "Guybrush.Threepwood"
        },
        {
            "auth_scheme": "native",
            "endpoint": {
                "ep_type": "irods",
                "hostname": "surf-yoda.myhost.nl",
                "port": 1247,
                "protocol": "irods",
                "zone": "yoda"
            },
            "metadata": {},
            "secret_type": "password",
            "tag": "surf-yoda.myhost.nl:1247::noaccess::native"
        }
    ],
    "provenance": []
}
```