#!/bin/sh

PROJ_DIR=neptune_project
SURFIE_CMD="surfie"
SURFIE_CONF=config.neptune.admin.ini


# adding iRODS provider 'lighthouse'
echo "adding lighthouse ..."
${SURFIE_CMD} --conf ${SURFIE_CONF} admin provider add --defaulttype irods --endpoints "irods::lighthouse.hosted.nl:1247" lighthouse
# adding Auth Management provider 'sram'
echo "adding sram ..."
${SURFIE_CMD} --conf ${SURFIE_CONF} admin provider add --defaulttype "web portal" --endpoints "web portal::sram.surf.nl:443" sram
# adding YODA provider 'portal-yoda'
echo "adding portal-yoda ..."
${SURFIE_CMD} --conf ${SURFIE_CONF} admin provider add --defaulttype irods --endpoints "irods::portal-yoda.irods.nl:1247" portal-yoda
# adding YODA provider 'test-yoda'
echo "adding test-yoda ..."
${SURFIE_CMD} --conf ${SURFIE_CONF} admin provider add --defaulttype irods --endpoints "irods::test-yoda.hosted.nl:1247" test-yoda
