#!/bin/sh

PROJ_DIR=neptune_project
SURFIE_CMD=surfie
SURFIE_CONF=config.neptune.admin.ini


# adding project 'Project9' to Neptune
${SURFIE_CMD} --conf ${SURFIE_CONF} admin project add --d 'a metadata explorer for the project 9' Project9 365
# setting members
${SURFIE_CMD} --conf ${SURFIE_CONF} admin project set Project9 members "metadata_explorer,project_reviewer"
# setting service_accounts
${SURFIE_CMD} --conf ${SURFIE_CONF} admin project set Project9 service_accounts "yoda_provider::portal-yoda,test_yoda_provider::test-yoda"
# setting sram_api_key
${SURFIE_CMD} --conf ${SURFIE_CONF} admin project set Project9 sram_api_key 'xxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxxx'
# setting sram_sender
${SURFIE_CMD} --conf ${SURFIE_CONF} admin project set Project9 sram_sender 'data team'
