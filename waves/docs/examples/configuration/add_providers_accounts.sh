#!/bin/sh

PROJ_DIR=neptune_project
SURFIE_CMD=surfie
SURFIE_CONF=config.neptune.admin.ini


# adding 'yoda_provider' provider account to Neptune
${SURFIE_CMD} --conf ${SURFIE_CONF} admin datauser add --role provider yoda_provider "changeme"
# adding 'metadata_explorer' provider account to Neptune
${SURFIE_CMD} --conf ${SURFIE_CONF} admin datauser add --role provider metadata_explorer "changeme"
# adding 'test_yoda_provider' provider account to Neptune
${SURFIE_CMD} --conf ${SURFIE_CONF} admin datauser add --role provider test_yoda_provider "changeme"
