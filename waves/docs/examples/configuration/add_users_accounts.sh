#!/bin/sh

PROJ_DIR=neptune_project
SURFIE_CMD=surfie
SURFIE_CONF=config.guts.neptune.admin.ini


# adding 'project_reviewer' reviewer account to Neptune
${SURFIE_CMD} --conf ${SURFIE_CONF} admin datauser add --role reviewer "project_reviewer" "changeme"
