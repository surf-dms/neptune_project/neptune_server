## Authentication flow: SRAM by invitation

### API perspective

1. Check if the user is registered:  
```commandline
curl -X 'GET' \
  'https://neptune.surf-hosted.nl/projects/<my project SRAM name>/sram/<alice.liddell%40surf.nl>' \
  -H 'accept: application/json'
```
2. If the user is not registered, invite the user:  
```commandline
curl -X 'POST' \
  'https://neptune.surf-hosted.nl/projects/<my project SRAM name>/sram/<alice.liddell%40surf.nl>' \
  -H 'accept: application/json' \
  -d ''
```
3. Delete the user registration upon request from the user: 
```commandline
curl -X 'DELETE' \
  'https://neptune.surf-hosted.nl/projects/<my project SRAM name>/sram/<alice.liddell%40surf.nl>' \
  -H 'accept: application/json'
```
### User perspective

The invitation flow from the perspective of the user is described here:  

https://servicedesk.surf.nl/wiki/display/IAM/Log+in+with+an+invitation