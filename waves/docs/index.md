# Table of Contents

- [Introduction](overview.md#introduction)
    - [Configuration](configuration.md)
    - [idea](modeling_the_infrastructure.md)
      - [Data at the center](modeling_the_infrastructure.md#data-at-the-center)
      - [Outer space](modeling_the_infrastructure.md#outer-space)
      - [Inner space](modeling_the_infrastructure.md#inner-space)
    - [shopping cart data flow](shopping_cart_flow/index.md)
    - [authentication flow: SRAM by invitation](auth_flow-sram-by-invitation.md)
      - [API perspective](auth_flow-sram-by-invitation.md#api-perspective)
      - [User perspective](auth_flow-sram-by-invitation.md#user-perspective)
    - [project example](project.md)