## Authorization

The following table shows which roles have access to which information.
The last line, related to `Data Provider 2 copies the data to the destination folder` indicates who has access to the data.  

|                                                                         | Web Portal service account | Data Steward reviewing the requests | User accessing the portal | Data Provider 2 service account | Data Provider 1 service account |
|-------------------------------------------------------------------------|----------------------------|-------------------------------------|---------------------------|---------------------------------|---------------------------------|
| Data Provider 1 shares the metadata with the Web Portal service account | yes                        | no                                  | yes                       | no                              | yes                             |
| The Web Portal requests data on behalf of the user                      | yes                        | yes                                 | no                        | no                              | yes                             |
| The requested data are shared with the Data Provider 2 service account  | no                         | no                                  | no                        | yes                             | yes                             |
| Data Provider 2 copies the data to the destination folder               | no                         | no                                  | yes                       | yes                             | yes                             |

