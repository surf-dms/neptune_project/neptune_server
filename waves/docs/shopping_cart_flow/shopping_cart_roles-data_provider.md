## Data Provider

The Data Provider shares and receives data.  
It means that the Data Provider has to be able to read and to write data.  
Now, usually, it is possible to define multiple separate accounts within the Data Provider system and give to some the 
permission to read and to others the permission to write.  
For example, we can assign one reader per project (Jim and James) and a single writer (Mary) to the whole Data Provider system.
This allows to minimize the risk of data leakages because in case one reader account is compromised, then only the data
of the related project is exposed. While the writer can receive or copy data from other data providers to any project in
the system.  
Naturally it is possible to limit the scope of the writer to only certain projects or create multiple writer accounts, one 
per project, but at the cost of an overhead in account and permission management.  
Finally, a reader account (Lucy) is needed for any other data provider which needs to copy data from the Data Provider.
The Data Provider in turn has a reader account in each of the other data providers. The reason for this last reader account 
is that the reader accounts of each project have to share the data with someone that has already access to the system.
This can be achieved in two ways: with a federated account or with a local account.
The key point is that those reader accounts mapped to each data provider have no access permission by default. 
They receive a temporary token to access certain project on demand.
So even if a data provider is compromised, the attacker has no access to any data, except in the case there is a temporary
token active in that moment, but that is still a risk limited in time and scope.

![Data Provider roles](../img/data_provider_roles.png "Data Provider roles")
