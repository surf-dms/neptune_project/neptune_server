## Cardinality

This paragraph aims to provide some indications abouts the number of operations per Data Provider that would be needed to
automate the shopping cart data flow.

| **Data Providers** | **share the metadata**   | **share the data**                  | **download**    |
|--------------------|--------------------------|-------------------------------------|-----------------|
| 1                  | surfie share with portal | surfie reshare with data provider 2 | surfie download |
|                    |                          | surfie reshare with data provider 3 |                 |
| 2                  | surfie share with portal | surfie reshare with data provider 1 | surfie download |
|                    |                          | surfie reshare with data provider 3 |                 |
| 3                  | surfie share with portal | surfie reshare with data provider 1 | surfie download |
|                    |                          | surfie reshare with data provider 2 |                 |

So, in total, 12 periodic tasks should be executed to implement the workflow for 3 Data Providers.
