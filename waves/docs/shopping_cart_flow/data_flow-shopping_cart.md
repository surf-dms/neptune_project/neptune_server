## Shopping cart data flow

![shopping cart data flow](../img/shoppin-cart_data-flow.png "Shopping cart data flow")

The above steps are mapped to the following roles:
![shopping cart roles](../img/shopping_cart_flow_roles.png "Shopping cart roles")
The "repository service" does multiple steps and it is described in more details [here](shopping_cart_roles-data_provider.md).  

1. share the metadata
2. discover the data
3. request access to the data
4. approve or reject the request
5. share the data
6. copy or download the data
7. acknowledge

The above steps are implemented in this way:  

1. #### share the metadata
```shell
surfie --conf config.provider.ini session --delta "7 days" share --noaccess --with web_portal_account --meta "json::/my/metadata/project-metadata.json,json::/research-folder/research-data-metadata.json" data-provider1::/project/dataset/research-data
```
```shell
curl -X 'POST' \
  'http://localhost:8080/session/' \
  -H 'accept: application/json' \
  -H 'Authorization: Basic c8VyZmKkbAluOm5pbWRhZnJ1cw==' \
  -H 'Content-Type: application/json' \
  -d '<see sharing metadata session example>'
```
[sharing metadata session example](../sharing_metadata_session.md)   

2. #### discover the data
    this step is expected to be implemented within a web portal or through other tools. Anyway Neptune allows to list the shared data:
```shell
surfie --conf config.web.portal.ini session list --v
sessions:
f129f9df-d4ae-4081-97e0-f57df6ab07af active shared 2025-01-18 12:31:02.102000 data_provider1_account
share /project/dataset/research-data from provider data-provider1
```
```shell
curl -X 'GET' \
  'http://localhost:8080/session/?json_search=%7B%22events.operation%22%3A%20%22share%22%7D' \
  -H 'accept: application/json' \
  -H 'Authorization: Basic c8VyZmKkbAluOm5pbWRhZnJ1cw=='  
```  
3. #### request access to the data
```shell
surfie --conf config.web.portal.ini session request --with "data_provider1_account,elizabeth" --by sram::guybrush.threepwood@surf.nl data-provider1::/project/dataset/research-data/data-object25
```
```commandline
curl -X 'POST' \
  'http://localhost:8080/session/' \
  -H 'accept: application/json' \
  -H 'Content-Type: application/json' \
  -d '<see requesting data session example>'
```  
[requesting data session example](../requesting_data_session_example.md)  
Notice that the request is done on behalf of a user called 'Guybrush Threepwood' identified through a provider with endpoint sram.surf.nl.  

4. #### approve or reject the request
```shell
surfie --conf config.reviewer.ini session list --type request --approve
```
The approval is done adding a line in the provenance field of the session including the request(s).  
```json lines
...
    "provenance": [
        "approved::LeChuck::2024-09-19 10:56:53.612357",
        ...
    ],
...
```
5. #### share the data
The data are shared posting a session similar to the example linked above.  
The important difference is that now the credentials to get access to the data are included.   
A target path may be included too, if it was requested by the user or the reviewer that has approved the request.  
System metadata should be included too when possible.
```shell
surfie --conf config.provider.ini session --text approved reshare --type request --from data-provider1 --to data-provider2 --with "data_provider2_account"
```
```json lines
    "events": [
        {
            "metadata": [
                [
                    "system",
                    "surf-yoda::/yoda/home/myproject/subdir0",
                    {
                        "checksum": {
                            "/yoda/home/myproject/subdir0/hello_yoda.txt": {
                                "sum": "e67a70d6d4275a85821dd7a4449dfc130f5a3cfe96bdc4ac0886780ddfe37b81",
                                "type": "sha256"
                            },
                            "/yoda/home/myproject/subdir0/subdir1/hello_irods.txt": {
                                "sum": "00fbb1007de1fc4a5a28c4a88103046dd6e2bb762ec769f9efbac5de8c2b9fe3",
                                "type": "sha256"
                            }
                        },
                        "size": 25,
                        "type": "collection"
                    }
                ]
            ],
            "operation": "share",
            "path": "/yoda/home/myproject/subdir0",
            "profile_tags": {
                "path": "surf-yoda.myhost.nl:1247::anonymous::native",
                "target_path": "surf-yoda.myhost.nl:1247::anonymous::native"
            },
            "target_path": "/yoda/home/public",
            "ticket": "mIDqcySTlbiueACCOOR"
        }
```
6. #### copy or download the data
Given the above information, anyone with the permission to read that session, with the path to the data, the data provider endpoint and the credentials to get access, can download or copy the data.
```shell
surfie --conf config.provider.ini session download --pt
```
Once the copy/download process is completed successfully, the client is expected to acknowledge that, adding a line to the session provenance field:  
```json lines
...
    "provenance": [
        "copied::provider2::2024-09-19 10:57:10.719887"
    ],
...
```
7. #### acknowledge
The data provider which has received the data request and shared the data is expected to acknowledge that the data have been shared and copied successfully.
```shell
surfie --conf config.provider.ini session acknowledge
```
This is done adding two lines in the provenance field of the original request session:  
```json lines
...
    "provenance": [
        "approved::LeChuck::2024-09-19 10:56:53.612357",
        "shared::surf_yoda_provider::2024-09-19 10:57:02.822115::57fe16a0-6ed4-4ea3-80ac-563b3674b2c3",
        "copied::surf_yoda_provider::2024-09-19 10:57:12.083795::57fe16a0-6ed4-4ea3-80ac-563b3674b2c3"
    ],
...
```
Notice that each of the two new lines includes, at the end, a uuid code.  
It is the code of the other session, the one that include the event with the operation 'share' and the credentials to 
get access to the data.  
The 'actors', in this workflow, that have made or reviewed the data request, should not need to get access to the data, 
so the information to do that is hidden from them, creating a separate session for sharing.  
They are not expected to have the permissions to read that session.  

The steps 1, 5, 6 and 7, can be automated, for example, running periodic tasks like explained in
[this page about automation](data_flow-automation.md).  
The various roles have different authorizations, according to the principle of minimal privileges. An example is
described [here](data_flow-shopping_cart-authorization.md).