## Shopping cart data flow automation

1. #### share the metadata
The Data Provider publishes regularly the metadata on Neptune, sharing them with any other party which is entitled to 
query them. This step can be automated for specific paths, running the following command in a script periodically:  
```shell
#!/bin/sh

surfie --conf config.provider.ini session --delta "7 days" share --noaccess --with web_portal_service_account --meta "json::/path/to/metadata/metadata.json,json::/path/to/metadata/more.metadata.json,json::/path/to/metadata/even.more.metadata.json" data-provider-friendly-name::/path/to/data-set
```
Where `surfie` is the Neptune client.  
The option `--conf config.provider.ini` provide the configuration to client with the credentials. See the config template.  
The option `--delta 7 days` makes the session active for 7 days, so that the other parties can discover it and read the 
metadata. After that time, the session expires and the metadata are not accessible anymore.  
The option `--with web_portal_service_account` specifies which other Neptune accounts can read that session and hence get 
access to the metadata.

2. #### share the data and acknowledge
The Data Provider can query periodically Neptune to discover if there are new data requests and share the requested data.
After sharing the data, it should acknowledge that the data have been shared by updating the provenance record embedded 
in the data request, so that the requestor can read it.  
This is an example of how both actions can be implemented in a shell script:
```shell
#!/bin/sh

surfie --conf config.provider.ini session --text approved reshare --type request --from data-provider-friendly-name --to data-provider-friendly-name-2 --with "data_provider_2_service_account"

surfie --conf config.provider.ini session acknowledge
```
The option `--text approved` means that only the requests including with the word "approved" are selected. The word "approved"
is added to the request in the provenance record when a data steward approves the request.  
The option `--type request` means that only the sessions of type 'request' are selected.  
The option `--from data-provider-friendly-name` means that only the requests of data hosted by 'data-provider-friendly-name'
are selected.  
The option `--to data-provider-friendly-name-2` means that only the requests of data to be copied to 'data-provider-friendly-name-2'
are selected.  
The option `--with "data_provider_2_service_account"` means that only the Neptune account with username 'data_provider_2_service_account'
will get access to the session including the credentials to download the data. Therefore, this account is very important
and it should be associated to the data provider 'data-provider-friendly-name-2' as a service account. This can be done
within the project definition (see [project](../project.md)).  
The first line of this script automates only the sharing of data from 'data-provider-friendly-name' to 'data-provider-friendly-name-2'.
In most cases multiple similar lines need to be added, two per each couple of data providers exchanging data (see [cardinality](cardinality.md)).  
The command on the second line takes care to update the provenance record of the original request, when the shared data 
are downloaded.  

3. #### copy or download the data
The Data Provider downloads the data shared with it, putting them in the folder specified in the request.
For example, with the following command executed periodically:
```shell
#!/bin/sh

surfie --conf config.provider.ini session download --pt
```
The option `--pt` means that the original directory tree structure is preserved.  
This script should be executed per Data Provider. Therefore, if the project has multiple Data Providers, there should be 
one periodic script per each of them. The configuration file must contain the credentials of the service account associated 
to the specific Data Provider.    
This script will also update the provenance record of the session where the data are shared, so that the Data Provider sharing
the data can see when they are copied and, in turn, update the provenance record of the request.