# Configuration
This page provides some indications about how to add to Neptune some of the metadata which describe Data Providers, Users
Data Users and Projects.

## Data Providers
To add a Data Provider, first it is necessary to add its description:
```shell
surfie --conf config.neptune.admin.ini admin provider add --defaulttype irods --endpoints "irods:myirods.myorg.nl:1247" data-repo
```
Where the option `--defaulttype` indicates the default type of Data Provider.  
The option `--endpoints` indicates the list of endpoints exposed by the Data Provider.  
The name 'data-repo' in the example is the "friendly name" used to identify the Data Provider within Neptune. It has to 
be unique.  
Once added the description, it is useful to add a service account for the Data Provider. An account that can be used to
do operations on behalf of the users, automating workflows:   
```shell
surfie --conf config.neptune.admin.ini admin datauser add data_repo_provider mypassword --role provider
```
The option `--role` defines the role of that Data User within a Data Provider.  
Notice that no Data Provider is specified. That means that the service account is added to Neptune itself. Neptune is a
Data Provider too. The role 'provider' determines which permissions has that Data User.

## Projects
It is possible to add a project with the following command:
```shell
surfie --conf config.neptune.admin.ini admin project add Triton 365
```
Then, after creating the project it is possible to add the mapping between Data Providers and service accounts:
```shell
surfie --conf config.neptune.admin.ini admin project set Triton service_accounts "data_repo_provider::data-repo,data_repo_provider2::data-repo2"
```
To set any other project's attribute the command is similar:
```shell
surfie --conf config.neptune.admin.ini admin project set Triton members "web_portal_account,ulysses,cook"
surfie --conf config.neptune.admin.ini admin project set Triton sram_sender "Data team"
```
Where the list of attributes of a project is defined in the file [schemas.py](https://gitlab.com/surf-dms/neptune_project/neptune_server/-/blob/main/waves/schemas.py?ref_type=heads#L52).

## Users

More examples, including the creation of users and service accounts, are available in the folder 
docs/examples/configuration.  
