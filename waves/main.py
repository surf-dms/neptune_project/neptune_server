import datetime
import os
import ssl
import secrets
import uuid

from fastapi import FastAPI, Depends, HTTPException, Query, status, Request
from fastapi.security import HTTPBasic, HTTPBasicCredentials
from fastapi.responses import RedirectResponse
from contextlib import asynccontextmanager
from typing import List

from typing_extensions import Annotated
from beanie import Document, PydanticObjectId
from functools import lru_cache

from . import schemas, crud, roles, helpers, events, guard, constants
from .database import init_db
from .config import config_dict
from .routers import providers, projects, users, data_users, data, metadata, relationship, session



@lru_cache
def get_settings():
    #return Settings()
    return config_dict['default']


#if get_settings().cert_path:
if get_settings()['cert_path']:
    ssl_context = ssl.SSLContext(ssl.PROTOCOL_TLS_SERVER)
    #ssl_context.load_cert_chain(get_settings().cert_path, keyfile=get_settings().private_key_path)
    ssl_context.load_cert_chain(get_settings()['cert_path'], keyfile=get_settings()['private_key_path'])

security_auth = HTTPBasic()


async def get_current_user(request: Request, credentials: Annotated[HTTPBasicCredentials, Depends(security_auth)]):
    #neptune_name = get_settings().neptune_server_friendly_name
    neptune_name = get_settings()['neptune_server_friendly_name']
    providers_list = list(await crud.get_providers_by_name(neptune_name))
    provider_db = next(iter(providers_list), None)
    data_user_db_list1 = await crud.get_data_user_by_username(credentials.username)
    data_user_db_list2 = await crud.get_data_user_by_provider_id(provider_db.id)
    data_user_db = None
    for data_user_db in data_user_db_list1:
        if data_user_db in data_user_db_list2:
            break
    if not data_user_db:
        is_correct_username = False
        is_correct_password = False
    else:
        is_correct_username = True
        is_correct_password = guard.hash_verify(data_user_db.password, credentials.password)

    if not (is_correct_username and is_correct_password):
        raise HTTPException(
            status_code=status.HTTP_401_UNAUTHORIZED,
            detail="Incorrect username or password",
            headers={"WWW-Authenticate": "Basic"},
        )
    request.state.data_user_db = data_user_db
    return data_user_db


@asynccontextmanager
async def lifespan(app: FastAPI):
    #await init_db(settings=get_settings())
    await init_db(config=get_settings())
    #await events.init_scheduler(settings=get_settings())
    await events.init_scheduler(config=get_settings())
    events.scheduler.start()
    yield
    events.scheduler.shutdown()


app = FastAPI(lifespan=lifespan, dependencies=[Depends(get_current_user)])

app.include_router(providers.router)
app.include_router(projects.router)
app.include_router(users.router)
app.include_router(data_users.router)
app.include_router(data.router)
app.include_router(metadata.router)
app.include_router(relationship.router)
app.include_router(session.router)


@app.get("/")
def read_root():
    return RedirectResponse("https://en.wikipedia.org/wiki/Neptune#/media/File:Neptune_Voyager2_color_calibrated.png")


# if __name__ == "__main__":
#    import uvicorn
#
#    uvicorn.run(app, host="0.0.0.0", port=8000)
